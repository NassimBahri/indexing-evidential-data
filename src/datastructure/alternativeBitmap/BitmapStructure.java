package datastructure.alternativeBitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BitmapStructure {

	public Map<Integer, Record> records;

	/**
	 * store all the distinct focal elements
	 * 
	 * @since we use the second index structure
	 */
	public ArrayList<Item> distinctFocalElements;

	/**
	 * default constructor of the BitmapStructure class
	 */
	public BitmapStructure() {
		this.records = new HashMap<Integer, Record>();
		this.distinctFocalElements = new ArrayList<Item>();
	}

	/**
	 * adding new record to the bitmap structure
	 * 
	 * @param key
	 * @param record
	 */
	public void add(Integer key, Record record) {
		this.records.put(key, record);
	}

	/**
	 * print bitmap structure as a string
	 */
	public String toString() {
		String ch = "";
		for (Entry<Integer, Record> cell : this.records.entrySet()) {
			ch += "Record N° " + cell.getKey() + "\n";
			ch += cell.getValue();
			ch += "\n--------------------\n";
		}
		return ch;
	}


}
