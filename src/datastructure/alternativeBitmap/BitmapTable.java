package datastructure.alternativeBitmap;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class BitmapTable extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Instance attributes used in this example
	private JPanel topPanel;
	private JScrollPane scrollPane;

	// Constructor of main frame
	public BitmapTable() {
		EvidentialBitmapOptimized bitmap = new EvidentialBitmapOptimized("datasets/trivial-data.txt", 1);
		// Set the frame characteristics
		setTitle("Bitmap");
		setSize(300, 200);
		setBackground(Color.gray);
		// Create a panel to hold all other components
		topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		getContentPane().add(topPanel);

		DefaultTableModel model = new DefaultTableModel();
		JTable table = new JTable(model);

		for (Object o : bitmap.subsets.toArray()) {
			model.addColumn(o.toString());
			System.out.println(o.toString());
		}

		for (Entry<Integer, Record> r : bitmap.bitmap.records.entrySet()) {
			ArrayList<Double> l = new ArrayList<Double>();

			for (Object o : bitmap.subsets.toArray()) {
				l.add(r.getValue().cells.get(o));
			}

			model.addRow(l.toArray());
		}

		// Add the table to a scrolling pane
		scrollPane = new JScrollPane(table);
		topPanel.add(scrollPane, BorderLayout.CENTER);
	}

	// Main entry point for this example
	public static void main(String args[]) {
		// Create an instance of the test application
		BitmapTable mainFrame = new BitmapTable();
		mainFrame.setVisible(true);
	}
}
