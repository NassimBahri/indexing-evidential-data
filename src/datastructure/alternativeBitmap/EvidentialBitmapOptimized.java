package datastructure.alternativeBitmap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import datastructure.util.Collection;

public class EvidentialBitmapOptimized {

	/**
	 * bitmap index
	 */
	public BitmapStructure bitmap;
	public Object[] singletons;
	public ArrayList<String> subsets;

	public static void main(String[] args) {
		EvidentialBitmapOptimized bitmap = new EvidentialBitmapOptimized("datasets/data5.txt", 1);
		/*
		 * for (String o : bitmap.subsets) { System.out.println(o.toString()); }
		 */
		System.out.println("---------------");
		// System.out.println(bitmap.bitmap);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<Item> res = bitmap.search("1,2,5", 0.0001);
		System.out.println("Nb : " + res.size());
		long stopTime = System.currentTimeMillis(); // end time
		System.out.println("Spent time : " + (stopTime - startTime)); // execution
																		// time

		// Collections.sort(res, new Comparator<Item>() {
		//
		// @Override
		// public int compare(Item o1, Item o2) {
		// if (o1.rowid < o2.rowid) {
		// return -1;
		// }
		// return 1;
		// }
		//
		// });
		// for (Item item : res) {
		// System.out.println(item);
		// }

	}

	public EvidentialBitmapOptimized(String filePath, int attribute) {
		this.initializeFoD(filePath, attribute);
		this.bitmap = new BitmapStructure();
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		Record record = null;
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				record = new Record();
				for (int i = 0; i < elementsList.length; i++) {
					if (currentAttribute == attribute) {
						double bel = Double.parseDouble(massList[i]);
						record.add(elementsList[i], bel);
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				this.bitmap.add(lineNumber, record);
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public void EvidentialBitmapOptimizedDep(String filePath, int attribute) {
		this.initializeFoD(filePath, attribute);
		this.bitmap = new BitmapStructure();
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		Record record = null;
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				record = new Record();
				for (int i = 0; i < elementsList.length; i++) {
					if (currentAttribute == attribute) {
						for (String subset : this.subsets) {
							double bel = 1;// this.caculateBelief(subset,
											// elementsList, massList,
											// attribute);
							record.add(subset, bel);
						}
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				this.bitmap.add(lineNumber, record);
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unused", "resource" })
	private void initializeFoD(String filePath, int attribute) {
		HashMap<String, Integer> elements = new HashMap<String, Integer>();
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					focalElementValues = elementsList[i].split(",");
					if (currentAttribute == attribute) {
						for (int j = 0; j < focalElementValues.length; j++) {
							String key = focalElementValues[j];
							if (elements.get(key) == null) {
								elements.put(key, 1);
							}
						}
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				lineNumber++;
			}
			this.singletons = elements.keySet().toArray();
			Arrays.sort(this.singletons);
			// generate subsets
			this.subsets = this.generatePowerSet(this.singletons);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generate all possible subsets from a focal element
	 * 
	 * @param items
	 *            list of items composing the focal element
	 * @return list of all possible subsets
	 */
	public ArrayList<String> generatePowerSet(Object[] items) {
		ArrayList<String> tempList = null, powerSet = new ArrayList<String>();
		for (Object item : items) {
			tempList = new ArrayList<String>();
			for (String temp : powerSet) {
				tempList.add(temp + "," + item);
			}
			powerSet.add(item.toString());
			powerSet.addAll(tempList);
		}
		return powerSet;
	}

	@SuppressWarnings("unused")
	private double caculateBelief(String element, String[] list, String[] mass, int attribute) {
		BigDecimal bel = new BigDecimal(0);
		BigDecimal currentMass = new BigDecimal(0);
		BigDecimal tmpMass = null;
		int i = 0;
		// get attribute
		int currentAttribute = 1;
		ArrayList<String> attributeMass = new ArrayList<String>(), attributeValues = new ArrayList<String>();
		for (String val : list) {
			if (currentAttribute == attribute) {
				attributeMass.add(mass[i]);
				attributeValues.add(val);
			}
			tmpMass = new BigDecimal(mass[i]);
			currentMass = currentMass.add(tmpMass);
			i++;
			if (currentMass.doubleValue() == 1) {
				currentAttribute++;
				currentMass = new BigDecimal(0);
			}
			if (currentAttribute > attribute) {
				break;
			}
		}
		i = 0;
		for (String item : attributeValues) {
			if (Collection.isSubsetOf(item.split(","), element.split(","))) {
				tmpMass = new BigDecimal(attributeMass.get(i));
				bel = bel.add(tmpMass);
			}
			i++;
		}
		return bel.doubleValue();
	}

	public ArrayList<Item> searchDep(String searched, double threshold) {
		ArrayList<Item> result = new ArrayList<Item>();
		for (Entry<Integer, Record> record : this.bitmap.records.entrySet()) {
			Double bel = record.getValue().cells.get(searched);
			if (bel != null && bel > threshold) {
				result.add(new Item(record.getKey(), bel));
			}
		}
		return result;
	}

	public ArrayList<Item> search(String searched, double threshold) {
		ArrayList<Item> result = new ArrayList<Item>();
		for (Entry<Integer, Record> record : this.bitmap.records.entrySet()) {
			ArrayList<String> subsets = this.generatePowerSet(searched.split(","));
			Double sum = 0.0, bel;
			for (String s : subsets) {
				bel = record.getValue().cells.get(s);
				if (bel != null) {
					sum += bel;
				}
			}
			if (sum > threshold) {
				result.add(new Item(record.getKey(), sum));
			}
		}
		return result;
	}

}
