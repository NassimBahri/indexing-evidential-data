package datastructure.alternativeBitmap;

/**
 * This class offers a structure to store the details of the focal elements
 * (belief value, the size of the focal element and the row identifier). Each
 * item refers to a line number with a belief value.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * row identifier of the focal element
	 */
	long rowid;
	/**
	 * belief of the focal element
	 */
	double belief;

	/**
	 * Custom constructor of the Result class
	 * 
	 * @param rowid
	 *            record identifier of the focal element
	 * @param belief
	 *            belief of the focal element in the corresponding rowid
	 */
	public Item(long rowid, double belief) {
		this.rowid = rowid;
		this.belief = belief;
	}

	/**
	 * Convert Result object to string
	 */
	public String toString() {
		return "(" + this.rowid + " , " + this.belief + ")";
	}
}