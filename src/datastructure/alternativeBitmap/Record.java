package datastructure.alternativeBitmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <strong>The Record class</strong> stores a list of cells where each one
 * correspond to a unique value from the domain of the indexed attribute.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Record {

	/**
	 * bitmap record/line
	 */
	public Map<String, Double> cells;

	/**
	 * Default constructor of the Record class
	 */
	public Record() {
		this.cells = new HashMap<String, Double>();
	}

	/**
	 * Verify if a key already exists in the record
	 * 
	 * @param key
	 *            a value from the domain of the indexed attribute
	 * @return true if the key exists and false in the opposite case
	 */
	public boolean keyExists(String key) {
		return (this.cells.containsKey(key)) ? true : false;
	}

	/**
	 * Add new item to the record
	 * 
	 * @param key
	 *            a value from the domain of the indexed attribute
	 * @param item
	 *            the item object to insert
	 */
	public void add(String key, Double item) {
		if (!this.keyExists(key)) {
			this.cells.put(key, item);
		}
	}

	/**
	 * Convert Record object to string
	 */
	public String toString() {
		String ch = "";
		for (Entry<String, Double> cell : this.cells.entrySet()) {
			ch += cell.getKey() + "\n";
			ch += cell.getValue();
			ch += "\n";
		}
		return ch;
	}

}
