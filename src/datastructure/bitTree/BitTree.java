package datastructure.bitTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <strong>The Bit Tree data structure</strong> is a tree based approach for
 * evidential data representation. The BIT contains n + 1 hierarchical levels,
 * where n refers to the number of at- tributes in the evidential database and
 * the level 0 witch is the root is the empty set ∅. Each level in the BIT
 * (except the first one), corresponds to an attribute from the evidential
 * database where we found all its distinct values.<br>
 * <br>
 * <strong>Reference:</strong> K. K. Hewawasam, K. Premaratne and M.-L. Shyu,
 * Rule mining and classification in a situation assessment application: A
 * belief-theoretic approach for handling data imperfections, <i>Trans. Sys. Man
 * Cyber. Part B 37</i> (December 2007) 1446–1459.<br>
 * <br>
 * <strong>Example of use :</strong> <br>
 * 
 * <pre>
 * // Create the Bit tree
 * BitTree bTree = new BitTree("datasets/dataset.txt");
 * </pre>
 * 
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class BitTree {

	/**
	 * root node of the tree
	 */
	public Node root = new Node("∅", 0);

	/**
	 * Constructor of the BitTree
	 * 
	 * @param filePath
	 *            the path to the data file
	 */
	@SuppressWarnings("resource")
	public BitTree(String filePath) {
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		int attribute = 1, currentAttribute = 1;
		double sum = 0;
		double mass = 0;
		Node child;
		HashMap<Integer, ArrayList<String>> previous = new HashMap<Integer, ArrayList<String>>();
		ArrayList<ArrayList<String>> possiblePaths = new ArrayList<ArrayList<String>>();
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			while ((elementsLine = buffer.readLine()) != null) {
				previous = new HashMap<Integer, ArrayList<String>>();
				attribute = 1;
				currentAttribute = 1;
				sum = 0;
				child = this.root;
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					if (attribute == 1) {
						child.addChild(new Node(elementsList[i], Double.parseDouble(massList[i])));
					} else {
						// generating all possible paths
						if (currentAttribute != attribute) {
							possiblePaths = new ArrayList<ArrayList<String>>();
							possiblePaths = calculatePaths(possiblePaths, previous, 1);
							currentAttribute = attribute;
						}
						// insert the node
						for (ArrayList<String> path : possiblePaths) {
							System.out.println("Attribure to insert : " + elementsList[i] + " : " + path);
							child = this.root;
							for (String element : path) {
								System.out.println(element);
								child = child.getNode(element);
							}
							mass = this.calculateMass(elementsList, massList, path);
							mass *= Double.parseDouble(massList[i]);
							child.addChild(new Node(elementsList[i], evidentialDatabase.Random.Round(mass, 2)));
						}
					}
					if (previous.get(attribute) == null) {
						previous.put(attribute, new ArrayList<String>());
					}
					previous.get(attribute).add(elementsList[i]);
					// increment the attribute level
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						attribute++;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generating all possible paths (cartesian product)
	 * 
	 * @param possiblePaths
	 *            all possible paths from the items belogns to the previous
	 *            attributes
	 * @param focalElements
	 *            previous focal element (corresponding to the previous
	 *            attributes)
	 * @param startAttribute
	 *            position of the attribute being treated
	 * @return all possible paths to insert a focal element
	 */
	@SuppressWarnings("serial")
	private ArrayList<ArrayList<String>> calculatePaths(ArrayList<ArrayList<String>> possiblePaths,
			HashMap<Integer, ArrayList<String>> focalElements, int startAttribute) {
		if (possiblePaths.size() == 0) {
			ArrayList<String> list1 = focalElements.get(startAttribute);
			ArrayList<String> list2 = focalElements.get(startAttribute + 1);
			if (list2 != null) {
				for (String item1 : list1) {
					ArrayList<String> line = new ArrayList<String>();
					line.add(item1);
					possiblePaths.add(line);
					for (String item2 : list2) {
						line.add(item2);
					}
				}
			} else {
				for (String item : list1) {
					possiblePaths.add(new ArrayList<String>() {
						{
							add(item);
						}
					});
				}
			}
		} else {
			ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();
			ArrayList<String> list2 = focalElements.get(startAttribute + 1);
			for (ArrayList<String> list1 : possiblePaths) {
				for (String item2 : list2) {
					ArrayList<String> line = new ArrayList<String>();
					temp.add(line);
					line.addAll(list1);
					line.add(item2);
				}
			}
			possiblePaths = temp;
		}
		if (startAttribute + 1 < focalElements.size()) {
			return this.calculatePaths(possiblePaths, focalElements, startAttribute + 1);
		}
		return possiblePaths;
	}

	/**
	 * Calculate the mass of an element
	 * 
	 * @param elementList
	 *            list of element includes in the path
	 * @param massList
	 *            mass list relative to the element list below
	 * @param path
	 *            paths to insert the treated focal element
	 * @return mass of the focal element
	 */
	private double calculateMass(String[] elementList, String[] massList, ArrayList<String> path) {
		double mass = 1;
		int i = 0;
		for (String element : path) {
			while (i < elementList.length) {
				if (element.equals(elementList[i])) {
					mass *= Double.parseDouble(massList[i]);
				}
				i++;
			}
		}
		return mass;
	}

}
