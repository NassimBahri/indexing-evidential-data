package datastructure.bitTree;

import java.util.ArrayList;

/**
 * This class represents a node of in a tree data structure. It has a label, a
 * mass value corresponding to the specified label and a list of children.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Node {

	/**
	 * focal element
	 */
	public String focalElement;
	/**
	 * the mass (the probability)
	 */
	public double mass;
	/**
	 * the node's children
	 */
	public ArrayList<Node> children;

	/**
	 * Default constructor of the class Item
	 */
	public Node() {
	}

	/**
	 * custom constructor for the current class
	 * 
	 * @param rowid
	 *            row identifier of the focal element
	 * @param mass
	 *            mass of the focal element in the specified line
	 */
	public Node(String focalElement, double mass) {
		this.focalElement = focalElement;
		this.mass = mass;
		this.children = new ArrayList<Node>();
	}

	/**
	 * Search for a node
	 * 
	 * @param value
	 *            the label of the node looking for
	 * @return a node object having the specified label
	 */
	public Node getNode(String value) {
		Node result = null;
		if (this.focalElement.equals(value)) {
			return this;
		} else {
			for (Node child : this.children) {
				result = child.getNode(value);
				if (result != null && result.focalElement.equals(value)) {
					return result;
				}
			}
			return result;
		}
	}

	/**
	 * Add new child to the current node
	 * 
	 * @param node
	 *            a node object to insert
	 */
	public void addChild(Node node) {
		boolean exist = false;
		for (Node child : this.children) {
			if (child.focalElement.equals(node.focalElement)) {
				child.mass += node.mass;
				exist = true;
				break;
			}
		}
		if (!exist) {
			this.children.add(node);
		}
	}

	/**
	 * Convert Node object to String
	 */
	public String toString() {
		String ch = "";
		ch += "(" + this.focalElement + " , " + this.mass + ")\n";
		if (this.children.size() > 0) {
			for (Node child : this.children) {
				ch += "|-" + child;
			}
		}
		return ch;
	}

}
