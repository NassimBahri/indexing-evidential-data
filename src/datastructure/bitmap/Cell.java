package datastructure.bitmap;

import java.util.ArrayList;

/**
 * <strong>The Cell class</strong> stores a list of couples for each entry.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Cell {

	/**
	 * list of items
	 */
	public ArrayList<Item> items;

	/**
	 * Default constructor of the class Cell
	 */
	public Cell() {
		this.items = new ArrayList<Item>();
	}

	/**
	 * Adding new item to the list
	 * 
	 * @param item
	 *            the item object to insert
	 */
	public void add(Item item) {
		this.items.add(item);
	}
}
