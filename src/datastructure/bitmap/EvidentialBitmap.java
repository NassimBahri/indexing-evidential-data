package datastructure.bitmap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import datastructure.util.Collection;

public class EvidentialBitmap {

	/**
	 * path to the file containing evidential data
	 */
	public String filePath;
	/**
	 * bitmap index
	 */
	public BitmapStructure bitmap;

	public static void main(String[] args) {
		EvidentialBitmap bitmapIndex = new EvidentialBitmap("datasets/trivial-data.txt", 1);
		System.out.println(bitmapIndex.bitmap);
		// ArrayList<Result> result = bitmapIndex.search("cancer,flu");
		// System.out.println("Result : " + result);
		// System.out.println(bitmapIndex.bitmap.distinctFocalElements);
		HashMap<String, Item> newline = new HashMap<String, Item>();
		newline.put("flu,anemia", new Item(0.7));
		newline.put("flu", new Item(0.3));
		bitmapIndex.insert(5, newline);
		System.out.println("-------------------------------------------");
		System.out.println(bitmapIndex.bitmap);
		HashMap<String, Item> deleteline = new HashMap<String, Item>();
		deleteline.put("flu,anemia", new Item(0.7));
		deleteline.put("flu", new Item(0.3));
		bitmapIndex.delete(5, deleteline);
		System.out.println("-------------------------------------------");
		System.out.println(bitmapIndex.bitmap);
	}

	/**
	 * constructor of the evidential bitmap index
	 * 
	 * @param fileName
	 * @param attribute
	 */
	@SuppressWarnings("resource")
	public EvidentialBitmap(String filePath, int attribute) {
		this.filePath = filePath;
		this.bitmap = new BitmapStructure();
		File file = new File(this.filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		Item item = null;
		Record record = null;
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				record = new Record();
				for (int i = 0; i < elementsList.length; i++) {
					focalElementValues = elementsList[i].split(",");
					item = new Item(i + 1, Double.parseDouble(massList[i]), focalElementValues.length);
					if (currentAttribute == attribute) {
						for (int j = 0; j < focalElementValues.length; j++) {
							record.add(focalElementValues[j], item);
						}
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				this.bitmap.add(lineNumber, record);
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * search value in the bitmap index
	 * 
	 * @param value
	 * @return
	 */
	public ArrayList<Result> search(String value) {
		String[] focalElements = value.split(",");
		int focalElementSize = focalElements.length;
		ArrayList<Result> result = new ArrayList<Result>();
		ArrayList<Item> tempList = new ArrayList<Item>(); // temporary list
		boolean exist = false;
		for (Entry<Integer, Record> record : this.bitmap.records.entrySet()) {
			for (Entry<String, Cell> cell : record.getValue().cells.entrySet()) {
				// if the searched value in singleton
				if (focalElementSize == 1 && cell.getKey().equals(value)) {
					// add all items where the size of the focal element is
					// equal to 1
					for (Item item : cell.getValue().items) {
						if (item.focalElementSize == 1) {
							result.add(new Result(record.getKey(), item.mass));
						}
					}
				} else if (focalElementSize > 1) {
					if (Collection.isSubsetOf(cell.getKey().split(","), focalElements)) {
						for (Item item : cell.getValue().items) {
							if (item.focalElementSize == 1) {
								// verify if the row id is already exist
								exist = false;
								for (Result res : result) {
									if (res.rowid == record.getKey()) {
										exist = true;
										res.belief += item.mass;
										break;
									}
								}
								if (!exist) {
									result.add(new Result(record.getKey(), item.mass));
								}
							} else if (focalElementSize >= item.focalElementSize) {
								tempList.add(item);
							}
						}
					}
				}
			}
			// treat the temporary list
			int i = 0, nb;
			while (i < tempList.size()) {
				nb = 0;
				for (Item item : tempList) {
					if (item.id == tempList.get(i).id && item.mass == tempList.get(i).mass
							&& item.focalElementSize == tempList.get(i).focalElementSize) {
						nb++;
					}
				}
				if (tempList.get(i).focalElementSize == nb) {
					exist = false;
					for (Result res : result) {
						if (res.rowid == record.getKey()) {
							exist = true;
							res.belief += tempList.get(i).mass;
							break;
						}
					}
					if (!exist) {
						result.add(new Result(record.getKey(), tempList.get(i).mass));
					}
				}
				tempList.remove(tempList.get(i));
			}
			tempList.clear();
		}
		return result;
	}

	/**
	 * insert new line on the bitmap index
	 * 
	 * @param rowsid
	 * @param items
	 */
	@SuppressWarnings("rawtypes")
	public void insert(int rowsid, Map<String, Item> items) {
		String[] focalElements;
		Record record = new Record();
		int position = 0;
		for (Entry item : items.entrySet()) {
			focalElements = item.getKey().toString().split(",");
			for (String element : focalElements) {
				record.add(element, new Item(position + 1, ((Item) item.getValue()).mass, focalElements.length));
			}
			position++;
		}
		this.bitmap.records.put(rowsid, record);
	}

	/**
	 * delete line from bitmap index
	 * 
	 * @param rowsid
	 * @param items
	 */
	public void delete(int rowsid, Map<String, Item> items) {
		this.bitmap.records.remove(rowsid);
	}

}
