package datastructure.bitmap;

/**
 * This class offers a structure to store the details of the focal elements
 * (belief value, the size of the focal element and the row identifier). Each
 * item refers to a line number with a belief value.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * position of the focal element in the bba
	 */
	public int id;
	/**
	 * mass of the focal element
	 */
	public double mass;
	/**
	 * size of the focal element
	 */
	public int focalElementSize;

	/**
	 * Default constructor of this class
	 */
	public Item() {
	}

	/**
	 * Custom constructor for the current class
	 * 
	 * @param id
	 *            position of the focal element in the bba
	 * @param mass
	 *            the mass of the item
	 * @param size
	 *            size of the focal element
	 */
	public Item(int id, double mass, int size) {
		this.id = id;
		this.mass = mass;
		this.focalElementSize = size;
	}

	/**
	 * Constructor with mass value
	 * 
	 * @param mass
	 *            mass of the focal element
	 */
	public Item(double mass) {
		this.mass = mass;
	}

	/**
	 * Convert an item object to String
	 */
	public String toString() {
		return "(" + Integer.toString(this.id) + " , " + Double.toString(this.mass) + " , "
				+ Integer.toString(this.focalElementSize) + ")";
	}

}
