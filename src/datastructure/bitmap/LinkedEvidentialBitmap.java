package datastructure.bitmap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map.Entry;

import datastructure.util.Collection;

public class LinkedEvidentialBitmap {

	/**
	 * bitmap index
	 */
	public BitmapStructure bitmap;


	/**
	 * constructor of the evidential bitmap index
	 * 
	 * @param fileName
	 * @param attribute
	 */
	@SuppressWarnings("resource")
	public LinkedEvidentialBitmap(String filePath, int attribute) {
		this.bitmap = new BitmapStructure();
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		Item item = null;
		Record record = null;
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				record = new Record();
				for (int i = 0; i < elementsList.length; i++) {
					focalElementValues = elementsList[i].split(",");
					item = new Item(i + 1, Double.parseDouble(massList[i]), focalElementValues.length);
					if (currentAttribute == attribute) {
						for (int j = 0; j < focalElementValues.length; j++) {
							record.add(focalElementValues[j], this.bitmap.addItem(item));
						}
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				this.bitmap.add(lineNumber, record);
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Result> search(String value, double threshold) {
		String[] focalElements = value.split(",");
		int focalElementSize = focalElements.length;
		ArrayList<Result> result = new ArrayList<Result>();
		ArrayList<Item> tempList = new ArrayList<Item>(); // temporary list
		boolean exist = false;
		boolean found = false;
		for (Entry<Integer, Record> record : this.bitmap.records.entrySet()) {
			// verify is at least the record contains one value from the items
			// composing the searched focal element
			found = false;
			for (String s : focalElements) {
				if (record.getValue().cells.containsKey(s)) {
					found = true;
					break;
				}
			}
			if (!found) {
				continue;
			}
			for (Entry<String, Cell> cell : record.getValue().cells.entrySet()) {
				// if the searched value in singleton
				if (focalElementSize == 1 && cell.getKey().equals(value)) {
					// add all items where the size of the focal element is
					// equal to 1
					for (Item item : cell.getValue().items) {
						if (item.focalElementSize == 1) {
							result.add(new Result(record.getKey(), item.mass));
						}
					}
				} else if (focalElementSize > 1) {
					if (Collection.isSubsetOf(cell.getKey().split(","), focalElements)) {
						for (Item item : cell.getValue().items) {
							if (item.focalElementSize == 1) {
								// verify if the row id is already exist
								exist = false;
								for (Result res : result) {
									if (res.rowid == record.getKey()) {
										exist = true;
										res.belief += item.mass;
										break;
									}
								}
								if (!exist) {
									result.add(new Result(record.getKey(), item.mass));
								}
							} else if (focalElementSize >= item.focalElementSize) {
								tempList.add(item);
							}
						}
					}
				}
			}
			// treat the temporary list
			int i = 0, nb;
			while (i < tempList.size()) {
				nb = 0;
				for (Item item : tempList) {
					if (item.id == tempList.get(i).id && item.mass == tempList.get(i).mass
							&& item.focalElementSize == tempList.get(i).focalElementSize) {
						nb++;
					}
				}
				if (tempList.get(i).focalElementSize == nb) {
					exist = false;
					for (Result res : result) {
						if (res.rowid == record.getKey()) {
							exist = true;
							res.belief += tempList.get(i).mass;
							break;
						}
					}
					if (!exist) {
						result.add(new Result(record.getKey(), tempList.get(i).mass));
					}
				}
				tempList.remove(tempList.get(i));
			}
			tempList.clear();
		}
		return result;
	}

}
