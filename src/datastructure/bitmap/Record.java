package datastructure.bitmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <strong>The Record class</strong> stores a list of cells where each one
 * correspond to a unique value from the domain of the indexed attribute.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Record {

	/**
	 * bitmap record/line
	 */
	public Map<String, Cell> cells;

	/**
	 * Default constructor of the Record class
	 */
	public Record() {
		this.cells = new HashMap<String, Cell>();
	}

	/**
	 * Verify if a key already exists in the record
	 * 
	 * @param key
	 *            a value from the domain of the indexed attribute
	 * @return true if the key exists and false in the opposite case
	 */
	public boolean keyExists(String key) {
		return (this.cells.containsKey(key)) ? true : false;
	}

	/**
	 * Add new item to the record
	 * 
	 * @param key
	 *            a value from the domain of the indexed attribute
	 * @param item
	 *            the item object to insert
	 */
	public void add(String key, Item item) {
		if (!this.keyExists(key)) {
			this.cells.put(key, new Cell());
		}
		// get the cell with the requested key then add the new item
		this.cells.get(key).add(item);
	}

	/**
	 * Convert Record object to string
	 */
	public String toString() {
		String ch = "";
		for (Entry<String, Cell> cell : this.cells.entrySet()) {
			ch += cell.getKey() + "\n";
			for (int i = 0; i < cell.getValue().items.size(); i++) {
				ch += cell.getValue().items.get(i);
			}
			ch += "\n";
		}
		return ch;
	}

}
