package datastructure.btree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import datastructure.util.Collection;

/**
 * <strong>BTree index for evidential data</strong>. This new structure will
 * allow as to create a belief based index for evidential data. It takes up the
 * fundamental concepts of the classic BTree (the order and the balancing
 * specifications,...) and add some customization to the non leaf nodes witch
 * will be able to store the minimum value and the maximum value of the belief
 * stored in their children.<br>
 * <strong>Reference:</strong> N. Bahri, M. A. B. Tobji, Efficient evidential
 * data structure, in <i>Complex Systems (WCCS), 2014 Second World
 * Conference</i> on Nov 2014, pp. 196– 201. <br>
 * <br>
 * <strong>Example of use :</strong>
 * 
 * <pre>
 * // Create the BTree
 * EvidentialBTree tree = new EvidentialBTree("dataset.txt", 1, 2);
 * 
 * // Insert new entry in the tree
 * ArrayList&lt;Item&gt; newLine = new ArrayList&lt;Item&gt;();
 * newLine.add(new Item("flu", 20, 0.99));
 * newLine.add(new Item("cancer", 20, 0.01));
 * tree.insert(tree.root,newLine)
 * 
 * // Deleting a line from the eTree
 * ArrayList&lt;Item&gt; oldLine = new ArrayList&lt;Item&gt;();
 * oldLine.add(new Item("flu", 20, 0.99));
 * oldLine.add(new Item("cancer", 20, 0.01));
 * tree.insert(tree.root,oldLine)
 * 
 * // Search all items with the corresponding properties
 * ArrayList&lt;Item&gt; listItems = tree.search(tree.root, "flu", 0.5, new ArrayList&lt;Item&gt;());
 * </pre>
 * 
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class EvidentialBTree {

	/**
	 * root node of the tree
	 */
	public Node root = new Node();
	/**
	 * order value of the tree
	 */
	public int order;
	/**
	 * store the path from the root node to the selected subtree
	 */
	private static ArrayList<Node> trace = new ArrayList<Node>();

	/**
	 * Constructs an evidential B-tree with the specified characteristics
	 * 
	 * @param filePath
	 *            the path to the data file
	 * @param attribute
	 *            position of the attribute to index
	 * @param order
	 *            the order of the tree
	 */
	@SuppressWarnings("resource")
	public EvidentialBTree(String filePath, int attribute, int order) {
		this.order = order;
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements and mass as list
		int currentAttribute = 1;
		BigDecimal sum = new BigDecimal(0);
		BigDecimal tmpSum = null;
		int lineNumber = 1, i;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				i = 0;
				for (String element : elementsList) {
					if (currentAttribute == attribute) {
						double bel = this.caculateBelief(element, elementsList, massList, attribute);
						this.insert(root, new Item(element, lineNumber, bel));
					}
					tmpSum = new BigDecimal(massList[i]);
					sum = sum.add(tmpSum);
					if (sum.doubleValue() == 1) {
						sum = new BigDecimal(0);
						currentAttribute++;
					}
					i++;
				}
				currentAttribute = 1; // reset the current attribute
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the subtree where the new entry must be inserted with the minimum
	 * penalty
	 * 
	 * @param root
	 *            the root node of the tree
	 * @param entry
	 *            the new entry to insert
	 * @return <tt>Node</tt> the subtree with the minimum penalty
	 */
	private Node chooseSubTree(Node root, Item entry) {
		Node subtree = null;
		// if the node root is a leaf (have no child)
		if (root.children.size() == 0) {
			return root;
		}
		int i = 0, count = root.children.size();
		Node next = null;
		for (Node child : root.children) {
			next = new Node();
			if (i < count - 1) {
				next = root.children.get(i + 1);
			}
			if (child.maxBel >= entry.belief && next.maxBel < entry.belief) {

				if (child.children.size() > 0) {
					EvidentialBTree.trace.add(child);
					return this.chooseSubTree(child, entry);
				}
				return child;
			}
			i++;
		}
		if (subtree == null) {
			subtree = root.children.get(count - 1);
		}
		return subtree;
	}

	/**
	 * Split a node in two and insert the new entry in the appropriate node.
	 * Then updates the boundaries of the nodes
	 * 
	 * @param root
	 *            the root node of the tree
	 * @param child
	 *            the node to split
	 * @param entry
	 *            the new entry to insert
	 */
	private void split(Node root, Node child, Item entry) {
		Node left = new Node(), right = new Node(), node = null;
		int center = this.order + 1, i = 0;
		boolean inserted = false;
		for (Item item : child.entries) {
			if (i < center) {
				node = left;
			} else {
				node = right;
			}
			if (inserted || item.belief > entry.belief) {
				node.addEntry(item);
			} else {
				node.addEntry(entry);
				if (i + 1 >= center) {
					node = right;
				}
				node.addEntry(item);
				inserted = true;
				i++;
			}
			i++;
		}
		if (!inserted) {
			right.addEntry(entry);
		}
		// update minBel and maxBel values of the nodes
		left.minBel = left.entries.get(left.entries.size() - 1).belief;
		left.maxBel = left.entries.get(0).belief;
		right.minBel = right.entries.get(right.entries.size() - 1).belief;
		right.maxBel = right.entries.get(0).belief;
		// update tree structure
		Node directParent = null;
		if (EvidentialBTree.trace.size() == 0) {
			directParent = root;
		} else {
			directParent = this.getParent();
		}
		directParent.entries.clear();
		int position = directParent.children.indexOf(child);
		if (position < 0) {
			position = 0;
		}
		directParent.children.add(position, left);
		directParent.children.add(position + 1, right);
		directParent.children.remove(child);
		// update minBel and maxBel values of the parent
		directParent.minBel = directParent.children.get(directParent.children.size() - 1).minBel;
		directParent.maxBel = directParent.children.get(0).maxBel;
		// verify the number of children
		int childrenCount = directParent.children.size();
		if (childrenCount > 2 * this.order) {
			Node parent = this.getParent();
			this.adjustKeys(directParent, parent);
		}
	}

	/**
	 * Split a saturated node and adjust the keys to keep the tree balanced
	 * 
	 * @param selected
	 *            current node to split
	 * @param parent
	 *            parent of the current node
	 */
	private void adjustKeys(Node selected, Node parent) {
		int i = this.order + 1;
		Node next = new Node();
		while (i < selected.children.size()) {
			next.children.add(selected.children.get(i));
			selected.children.remove(i);
		}
		// update the minBel value of the overdriven node
		selected.minBel = selected.children.get(selected.children.size() - 1).minBel;
		// update the boundaries of the new node
		next.minBel = next.children.get(next.children.size() - 1).minBel;
		next.maxBel = next.children.get(0).maxBel;
		// trivial condition the saturated node is the root
		if (selected == parent) {
			Node newRoot = new Node();
			newRoot.children.add(selected);
			newRoot.children.add(next);
			// update the boundaries of the new root
			newRoot.minBel = newRoot.children.get(newRoot.children.size() - 1).minBel;
			newRoot.maxBel = newRoot.children.get(0).maxBel;
			this.root = newRoot;
		} else {
			// the saturated node is not the root
			int position = parent.children.indexOf(selected);
			if (position < 0) {
				position = 0;
			}
			parent.children.add(position + 1, next);
			// update minBel and maxBel values of the parent node
			parent.minBel = parent.children.get(parent.children.size() - 1).minBel;
			parent.maxBel = parent.children.get(0).maxBel;
			// verify the number of children
			if (parent.children.size() > 2 * this.order) {
				this.adjustKeys(parent, this.getParent());
			}
		}
	}

	/**
	 * Calculate the belief of a focal element
	 * 
	 * @param element
	 *            focal element value
	 * @param list
	 *            list of focal elements in a line
	 * @param mass
	 *            list of mass in a line
	 * @return <tt>double</tt> the belief of the focal element
	 */
	private double caculateBelief(String element, String[] list, String[] mass, int attribute) {
		BigDecimal bel = new BigDecimal(0);
		BigDecimal currentMass = new BigDecimal(0);
		BigDecimal tmpMass = null;
		int i = 0;
		// get attribute
		int currentAttribute = 1;
		ArrayList<String> attributeMass = new ArrayList<String>(), attributeValues = new ArrayList<String>();
		for (String val : list) {
			if (currentAttribute == attribute) {
				attributeMass.add(mass[i]);
				attributeValues.add(val);
			}
			tmpMass = new BigDecimal(mass[i]);
			currentMass = currentMass.add(tmpMass);
			i++;
			if (currentMass.doubleValue() == 1) {
				currentAttribute++;
				currentMass = new BigDecimal(0);
			}
			if (currentAttribute > attribute) {
				break;
			}
		}
		i = 0;
		for (String item : attributeValues) {
			if (Collection.isSubsetOf(item.split(","), element.split(","))) {
				tmpMass = new BigDecimal(attributeMass.get(i));
				bel = bel.add(tmpMass);
			}
			i++;
		}
		return bel.doubleValue();
	}

	/**
	 * Get the parent of the selected node
	 * 
	 * @return <tt>Node</tt> the parent of the current node
	 */
	private Node getParent() {
		Node parent = null;
		if (EvidentialBTree.trace.size() > 0) {
			parent = EvidentialBTree.trace.get(EvidentialBTree.trace.size() - 1);
			EvidentialBTree.trace.remove(parent);
		} else {
			parent = this.root;
		}
		return parent;
	}

	/**
	 * Insert new entry to the tree. Find where the entry should go, and add it,
	 * then split if necessary
	 * 
	 * @param root
	 *            the root node of the tree
	 * @param entry
	 *            the new entry to insert
	 */
	private void insert(Node root, Item entry) {
		// clear trace
		EvidentialBTree.trace.clear();
		Node subtree = this.chooseSubTree(root, entry);
		// verify if there is a place for the new entry
		if (subtree.entries.size() + 1 <= 2 * this.order) {
			subtree.addEntry(entry);
			// update the boundaries of the trace
			for (Node trace : EvidentialBTree.trace) {
				if (trace.minBel == 0 || trace.minBel > entry.belief) {
					trace.minBel = entry.belief;
				}
				if (trace.maxBel < entry.belief) {
					trace.maxBel = entry.belief;
				}
			}
			// update the boundaries of the root node
			if (this.root.minBel == 0 || this.root.minBel > entry.belief) {
				this.root.minBel = entry.belief;
			}
			if (this.root.maxBel < entry.belief) {
				this.root.maxBel = entry.belief;
			}
		} else {
			this.split(this.root, subtree, entry);
		}
	}

	/**
	 * Insert new line in the tree
	 * 
	 * @param root
	 *            the root node of the tree
	 * @param entries
	 *            list of items
	 */
	public void insert(Node root, ArrayList<Item> entries) {
		for (Item entry : entries) {
			System.out.println("add " + entry.focalElement);
			this.insert(root, entry);
		}
	}

	/**
	 * Delete a line from the tree
	 * 
	 * @param root
	 *            the root of the tree
	 * @param entries
	 *            list of items
	 */
	public void delete(Node root, ArrayList<Item> entries) {
		Node subtree = null;
		String[] focalElements;
		int i;
		for (Item entry : entries) {
			EvidentialBTree.trace.clear();
			focalElements = entry.focalElement.split(",");
			Arrays.sort(focalElements);
			entry.focalElement = String.join(",", focalElements);
			subtree = this.chooseSubTree(root, entry);
			i = 0;
			while (i < subtree.entries.size()) {
				if (subtree.entries.get(i).focalElement.equals(entry.focalElement)
						&& subtree.entries.get(i).rowid == entry.rowid) {
					subtree.entries.remove(subtree.entries.get(i));
					break;
				}
				i++;
			}
			if (entry.belief == subtree.maxBel) {
				subtree.maxBel = subtree.entries.get(0).belief;
			}
			if (entry.belief == subtree.minBel) {
				subtree.minBel = subtree.entries.get(subtree.entries.size() - 1).belief;
			}
			for (Node ascendent : EvidentialBTree.trace) {
				if (entry.belief == ascendent.maxBel) {
					ascendent.maxBel = ascendent.children.get(0).maxBel;
				}
				if (entry.belief == ascendent.minBel) {
					ascendent.minBel = ascendent.children.get(ascendent.children.size() - 1).maxBel;
				}
			}
			if (entry.belief == root.maxBel) {
				root.maxBel = root.children.get(0).maxBel;
			}
			if (entry.belief == root.minBel) {
				root.minBel = root.children.get(root.children.size() - 1).maxBel;
			}
			if (subtree.entries.size() < this.order) {
				this.merge(subtree);
			}
		}
	}

	/**
	 * Merge the entries / children of two adjacent nodes
	 * 
	 * @param selected
	 *            selected node
	 */
	private void merge(Node selected) {
		int index, counter, center;
		String attribute = "children";
		if (selected.entries.size() > 0) {
			attribute = "entries";
		}
		Node parent = this.getParent(), previousNode = null, nextNode = null;
		int previousSize = 0, nextSize = 0, selectedSize = selected.get(attribute).size();
		int position = parent.children.indexOf(selected);
		if (position > 0) {
			previousNode = (Node) parent.children.get(position - 1);
			previousSize = previousNode.get(attribute).size();
		}
		if (position < parent.children.size() - 1) {
			nextNode = (Node) parent.children.get(position + 1);
			nextSize = nextNode.get(attribute).size();
		}
		if (previousNode != null && previousSize + selectedSize <= 2 * this.order) {
			index = 0;
			counter = previousSize;
			while (index < selected.get(attribute).size()) {
				if (selected.entries.size() > 0) {
					previousNode.entries.add(counter, selected.entries.get(index));
				} else {
					previousNode.children.add(counter, selected.children.get(index));
				}
				counter++;
				selected.get(attribute).remove(selected.get(attribute).get(index));
			}
			previousNode.updateBoundaries();
			parent.children.remove(selected);
			parent.updateBoundaries();
			if ((EvidentialBTree.trace.size() > 0 || attribute.equals("entries"))
					&& parent.children.size() < this.order) {
				System.out.println("there");
				this.merge(parent);
			}
		} else if (nextNode != null && nextSize + selectedSize <= 2 * this.order) {
			index = 0;
			counter = selectedSize;
			while (index < nextNode.get(attribute).size()) {
				if (selected.entries.size() > 0) {
					selected.entries.add(counter, nextNode.entries.get(index));
				} else {
					selected.children.add(counter, nextNode.children.get(index));
				}
				counter++;
				nextNode.get(attribute).remove(nextNode.get(attribute).get(index));
			}
			selected.updateBoundaries();
			parent.children.remove(nextSize);
			parent.updateBoundaries();
			if ((EvidentialBTree.trace.size() > 0 || attribute.equals("entries"))
					&& parent.children.size() < this.order) {
				this.merge(parent);
			}
		} else if (previousNode != null) {
			center = ((previousSize + selectedSize) / 2);
			index = center + 1;
			counter = 0;
			while (index < previousNode.get(attribute).size()) {
				if (selected.entries.size() > 0) {
					selected.entries.add(counter, previousNode.entries.get(index));
				} else {
					selected.children.add(counter, previousNode.children.get(index));
				}
				counter++;
				previousNode.get(attribute).remove(previousNode.get(attribute).get(index));
			}
			selected.updateBoundaries();
			previousNode.updateBoundaries();
		} else if (nextNode != null) {
			center = ((nextSize + selectedSize) / 2);
			index = 0;
			counter = selectedSize;
			while (counter < center + 1) {
				if (selected.entries.size() > 0) {
					selected.entries.add(counter, nextNode.entries.get(index));
				} else {
					selected.children.add(counter, nextNode.children.get(index));
				}
				counter++;
				nextNode.get(attribute).remove(nextNode.get(attribute).get(index));
			}
			selected.updateBoundaries();
			nextNode.updateBoundaries();
		}
	}

	/**
	 * Insert new entry to the existing tree
	 * 
	 * @param root
	 *            the root of the tree
	 * @param value
	 *            the value of the focal element
	 * @param threshold
	 *            minimum belief that should be satisfied
	 * @param result
	 *            store the result
	 * @return <tt>ArrayList&lt;Item&gt;</tt> list of items satisfying the query
	 *         properties
	 */
	public ArrayList<Item> search(Node root, String value, double threshold, ArrayList<Item> result) {
		if (root.maxBel < threshold) {
			return result;
		}
		// if the node is a leaf
		if (root.children.size() == 0) {
			for (Item item : root.entries) {
				if (item.belief < threshold) {
					break;
				}
				if (item.focalElement.equals(value)) {
					result.add(item);
				}
			}
		}
		// if the node is a non leaf
		for (Node child : root.children) {
			if (child.maxBel >= threshold) {
				result = this.search(child, value, threshold, result);
			}
			if (child.minBel < threshold) {
				break;
			}
		}
		return result;
	}

	/**
	 * delete after debug
	 * 
	 * @param node
	 *            node to print
	 */
	@SuppressWarnings("unused")
	private void toStringMessage(Node node) {
		System.out.println("Node : (" + node.maxBel + "," + node.minBel + ")");
		if (node.children.size() > 0) {
			System.out.println("children : [");
		}
		for (Node n : node.children) {
			this.toStringMessage(n);
		}
		if (node.children.size() > 0) {
			System.out.println("]");
		}
	}

	/**
	 * to delete after debug
	 */
	@SuppressWarnings("unused")
	private void debugTrace() {
		for (Node n : EvidentialBTree.trace) {
			System.out.println("trace node " + n.maxBel + " , " + n.minBel);
			// this.toStringMessage(n);
		}
	}

}
