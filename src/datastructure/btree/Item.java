package datastructure.btree;

/**
 * This class offers a structure to store the details of the focal elements (belief
 * value and the row number). Each item represents an entry in the leaf nodes.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * Value of the focal element
	 */
	public String focalElement = "";
	/**
	 * Row id of the focal element
	 */
	public long rowid = 0;
	/**
	 * The belief of the focal element
	 */
	public double belief = 0;

	/**
	 * Constructs an empty object
	 */
	public Item() {

	}

	/**
	 * Constructs an item with specified values for its attributes
	 * 
	 * @param focalElement
	 *            value of the focal element
	 * @param rowid
	 *            the row number
	 * @param belief
	 *            belief value of the focal element
	 */
	public Item(String focalElement, long rowid, double belief) {
		this.focalElement = focalElement;
		this.rowid = rowid;
		this.belief = belief;
	}

}
