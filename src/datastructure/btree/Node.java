package datastructure.btree;

import java.util.ArrayList;

/**
 * This class represents a node of in a tree data structure. It has a label, a
 * set of items corresponding to the specified label and a list of children.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Node {

	/**
	 * minimum value of the belief
	 */
	public double minBel = 0;
	/**
	 * maximum value of the belief
	 */
	public double maxBel = 0;
	/**
	 * children of the current node
	 */
	public ArrayList<Node> children = new ArrayList<Node>();
	/**
	 * node entries
	 */
	public ArrayList<Item> entries = new ArrayList<Item>();

	/**
	 * Constructs an empty node
	 */
	public Node() {
	}

	/**
	 * Getting the entries or children of a node
	 * 
	 * @param attribute
	 *            takes two possible values from the list{entries,children}
	 * @return the children or the entries of the selected nose
	 */
	public ArrayList<?> get(String attribute) {
		if (attribute.equals("entries")) {
			return this.entries;
		}
		return this.children;
	}

	/**
	 * Add new entry to the selected node and update the boundary interval of
	 * the node
	 * 
	 * @param entry
	 *            the new entry to insert
	 */
	public void addEntry(Item entry) {
		if (this.entries.size() == 0) {
			this.entries.add(entry);
		} else {
			ArrayList<Item> temp = new ArrayList<Item>();
			boolean inserted = false;
			for (Item item : this.entries) {
				if (inserted) {
					temp.add(item);
					continue;
				}
				if (item.belief > entry.belief) {
					temp.add(item);
				} else {
					temp.add(entry);
					temp.add(item);
					inserted = true;
				}
			}
			if (!inserted) {
				temp.add(entry);
			}
			this.entries = temp;
		}
		if (this.minBel == 0 || entry.belief < this.minBel) {
			this.minBel = entry.belief;
		}
		if (entry.belief > this.maxBel) {
			this.maxBel = entry.belief;
		}
	}

	/**
	 * Update the non leaf node boundaries
	 */
	public void updateBoundaries() {
		if (this.entries.size() > 0) {
			this.maxBel = this.entries.get(0).belief;
			this.minBel = this.entries.get(this.entries.size() - 1).belief;
		} else {
			this.maxBel = this.children.get(0).maxBel;
			this.minBel = this.children.get(this.children.size() - 1).minBel;
		}
	}

	/**
	 * Convert Node object to String
	 * 
	 * @return <tt>String</tt> the attribute's values as string
	 */
	public String toString() {
		String ch = "{";
		ch += "minBel : " + this.minBel + " , maxBel : " + this.maxBel + " items : [";
		for (Item item : this.entries) {
			ch += "(" + item.focalElement + "," + item.belief + "),";
		}
		return ch + "]}";
	}

}
