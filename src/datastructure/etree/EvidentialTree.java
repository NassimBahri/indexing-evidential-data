package datastructure.etree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import datastructure.util.Collection;

/**
 * <strong>The Evidential Tree (eTree) data structure</strong> is a tree based
 * approach for evidential data indexing. This structure stores the content of
 * only one evidential attribute <strong><i>A</i></strong>.<br>
 * This class allows to create an eTree for only one attribute from the
 * evidential database by specifying its position <br>
 * <br>
 * <strong>Reference:</strong> A. Jammali, M. A. B. Tobji, A. Martin and B. B.
 * Yaghlane, Indexing evidential data, in <i>Complex Systems (WCCS), 2014 Second
 * World Conference</i> on Nov 2014, pp. 196– 201. <br>
 * <br>
 * <strong>Example of use :</strong> <br>
 * 
 * <pre>
 * // Create the eTree
 * EvidentialTree eTree = new EvidentialTree("dataset.txt", 1);
 * 
 * // Insert new line in the eTree
 * HashMap&lt;String, Item&gt; newline = new HashMap&lt;String, Item&gt;();
 * newline.put("flu,anemia", new Item(5, 0.7));
 * newline.put("flu", new Item(5, 0.3));
 * eTree.insert(newline);
 * 
 * // Deleting a line from the eTree
 * HashMap&lt;String, Item&gt; oldline = new HashMap&lt;String, Item&gt;();
 * oldline.put("flu,anemia", new Item(5, 0.7));
 * oldline.put("flu", new Item(5, 0.3));
 * eTree.delete(oldline);
 * 
 * // Search all items with the corresponding properties
 * ArrayList&lt;Item&gt; listItems = eTree.search("flu");
 * </pre>
 * 
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class EvidentialTree {

	/**
	 * root node of the evidential tree
	 */
	public Node root;

	/**
	 * Constructor of the class EvidentialTree
	 * 
	 * @param filePath
	 *            the path to the data file
	 * @param attribute
	 *            position of the attribute to treat (we start counting from 1)
	 */
	@SuppressWarnings("resource")
	public EvidentialTree(String filePath, int attribute) {
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
										// the file
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		int currentAttribute = 1;
		double sum = 0;
		this.root = new Node();
		Node child;
		Item item;
		ArrayList<Item> list;
		String nextValues = ""; // next value on of the ith value
								// is a composed
		// focal element
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					if (currentAttribute == attribute) {
						// calculate the size of the focal element
						focalElementValues = elementsList[i].split(",");
						Arrays.sort(focalElementValues);
						// Arrays.sort(focalElementValues,new
						// Comparator<String>() {
						// @Override
						// public int compare(String o1, String o2) {
						// if(Double.parseDouble(o1)<Double.parseDouble(o2)){
						// return -1;
						// }
						// return 1;
						// }
						// });
						// if the focal element is a singleton
						if (focalElementValues.length == 1) {
							item = new Item(lineNumber, Double.parseDouble(massList[i]));
							list = new ArrayList<Item>();
							list.add(item);
							child = new Node(elementsList[i].toLowerCase(), list);
							this.root.addChild(child);
						} else {
							// the focal element is composed
							child = this.root;
							int index = 0;
							for (String val : focalElementValues) {
								if (index < focalElementValues.length - 1) {
									nextValues = focalElementValues[index + 1];
								} else {
									nextValues = "";
								}
								child = child.addElement(val.toLowerCase(),
										new Item(lineNumber, Double.parseDouble(massList[i])), nextValues);
								index++;
							}
							Node.previousItems.clear();// clear the stored items
						}

					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Searching for a value in the eTree
	 * 
	 * @param value
	 *            focal element to search (specified in the WHERE clause)
	 * @param threshold
	 *            minimum belief
	 * @return a list of items that point to the affected lines
	 */
	public ArrayList<Item> search(String value, double threshold) {
		value = value.toLowerCase();
		String[] focalElements = value.split(","), childFocalElements;
		ArrayList<Item> resultedLines = new ArrayList<Item>();
		// if the searched value is a singleton
		if (focalElements.length == 1) {
			for (Node child : this.root.children) {
				if (child.value.equals(value)) {
					// resultedLines.addAll(child.items);
					for (Item item : child.items) {
						if (item.belief >= threshold) {
							resultedLines.add(item);
						}
					}
					break;
				}
			}
			return resultedLines;
		} else {
			// if the focal element is composed
			for (Node child : this.root.children) {
				childFocalElements = child.value.split(",");
				if (Collection.isSubsetOf(childFocalElements, focalElements)) {
					int j = 0;
					boolean exists = false;
					for (Item item : child.items) {
						j = 0;
						exists = false;
						while (j < resultedLines.size()) {
							if (item.rowid == resultedLines.get(j).rowid) {
								resultedLines.get(j).belief += item.belief;
								exists = true;
							}
							j++;
						}
						if (!exists) {
							resultedLines.add(new Item(item.rowid, item.belief));
						}
					}
					this.search(child, value, resultedLines);
				}
			}
		}
		// remove
		int i = 0;
		while (i < resultedLines.size()) {
			if (resultedLines.get(i).belief < threshold) {
				resultedLines.remove(i);
			} else {
				i++;
			}
		}
		return resultedLines;
	}

	/**
	 * Recursive method for searching node
	 * 
	 * @param node
	 *            the selected node
	 * @param value
	 *            the label of the searched node
	 * @param resultedLines
	 *            list of couples contained in the searched node
	 * @return list of couples contained in the searched node
	 */
	private ArrayList<Item> search(Node node, String value, ArrayList<Item> resultedLines) {
		String[] focalElements = value.split(","), childFocalElements;
		for (Node child : node.children) {
			childFocalElements = child.value.split(",");
			if (Collection.isSubsetOf(childFocalElements, focalElements)) {
				// update the item's belief
				int j = 0;
				boolean exists = false;
				for (Item item : child.items) {
					j = 0;
					exists = false;
					while (j < resultedLines.size()) {
						if (item.rowid == resultedLines.get(j).rowid) {
							resultedLines.get(j).belief += item.belief;
							exists = true;
						}
						j++;
					}
					if (!exists) {
						resultedLines.add(new Item(item.rowid, item.belief));
					}
				}
				this.search(child, value, resultedLines);
			}
		}
		return resultedLines;
	}

	/**
	 * Insert new line in the eTree
	 * 
	 * @param items
	 *            list of items corresponding to a specified focal element
	 */
	@SuppressWarnings("rawtypes")
	public void insert(Map<String, Item> items) {
		String[] focalElements;
		ArrayList<Item> list;
		Node child;
		String nextValues = "";
		for (Entry item : items.entrySet()) {
			focalElements = item.getKey().toString().split(",");
			Arrays.sort(focalElements);
			if (focalElements.length == 1) {
				list = new ArrayList<Item>();
				list.add(new Item(((Item) item.getValue()).rowid, ((Item) item.getValue()).belief));
				child = new Node(item.getKey().toString(), list);
				this.root.addChild(child);
			} else {
				child = this.root;
				int index = 0;
				for (String val : focalElements) {
					if (index < focalElements.length - 1) {
						nextValues = focalElements[index + 1];
					} else {
						nextValues = "";
					}
					child = child.addElement(val,
							new Item(((Item) item.getValue()).rowid, ((Item) item.getValue()).belief), nextValues);
					index++;
				}
				Node.previousItems.clear();// clear the stored items
			}
		}
	}

	/**
	 * Deleting a line from the eTree
	 * 
	 * @param items
	 *            list of items corresponding to a specified focal element
	 */
	@SuppressWarnings("rawtypes")
	public void delete(Map<String, Item> items) {
		String[] focalElements;
		Node child;
		for (Entry item : items.entrySet()) {
			focalElements = item.getKey().toString().split(",");
			Arrays.sort(focalElements);
			if (focalElements.length == 1) {
				child = this.root.getNode(item.getKey().toString());
				int i = 0;
				while (i < child.items.size()) {
					if (child.items.get(i).rowid == ((Item) item.getValue()).rowid) {
						child.items.remove(child.items.get(i));
					}
					i++;
				}
			} else {
				child = this.root;
				int index = 1;
				for (String val : focalElements) {
					child = child.getNode(val);
					if (index == focalElements.length) {
						int i = 0;
						while (i < child.items.size()) {
							if (child.items.get(i).rowid == ((Item) item.getValue()).rowid) {
								child.items.get(i).belief -= ((Item) item.getValue()).belief;
							}
							if (child.items.get(i).belief == 0) {
								child.items.remove(child.items.get(i));
							}
							i++;
						}
					}
					index++;
				}
			}
		}
	}

}
