package datastructure.etree;

/**
 * This class offers a structure to store the details of the focal elements
 * (belief value and the row identifier). Each item refers to a line number with
 * a belief value.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * row identifier of the focal element
	 */
	public int rowid;
	/**
	 * the belief of the focal element
	 */
	public double belief;

	/**
	 * Default constructor of the class Item
	 */
	public Item() {
	}

	/**
	 * Custom constructor for the current class
	 * 
	 * @param rowid
	 *            the row identifier of the item
	 * @param belief
	 *            the belief of the item
	 */
	public Item(int rowid, double belief) {
		this.rowid = rowid;
		this.belief = belief;
	}

	/**
	 * Convert an item object to String
	 */
	public String toString() {
		String ch = "";
		ch += "(" + this.rowid + " , " + this.belief + ")";
		return ch;
	}

}
