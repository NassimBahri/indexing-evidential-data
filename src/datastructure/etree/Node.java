package datastructure.etree;

import java.util.ArrayList;

/**
 * This class represents a node of in a tree data structure. It has a label, a
 * set of items corresponding to the specified label and a list of children.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Node {

	/**
	 * value of the stored element
	 */
	public String value;
	/**
	 * RID list corresponding to the label
	 */
	public ArrayList<Item> items;
	/**
	 * the node's children
	 */
	public ArrayList<Node> children;
	/**
	 * store all items included in the path of the current value (for composed
	 * focal element)
	 */
	public static ArrayList<Item> previousItems = new ArrayList<Item>();

	/**
	 * Default constructor of the class Node
	 */
	public Node() {
		this.value = "";
		this.items = new ArrayList<Item>();
		this.children = new ArrayList<Node>();
	}

	/**
	 * Custom constructor of the class Node
	 * 
	 * @param value
	 *            label of the node
	 * @param items
	 *            list of entries
	 */
	public Node(String value, ArrayList<Item> items) {
		this.value = value.trim();
		this.items = items;
		this.children = new ArrayList<Node>();
	}

	/**
	 * Add new child to the current node
	 * 
	 * @param node
	 *            a node object to insert
	 */
	public void addChild(Node node) {
		boolean exist = false;
		for (Node child : this.children) {
			if (child.value.equals(node.value)) {
				for (Item item : node.items) {
					child.items.add(item);
				}
				exist = true;
				break;
			}
		}
		if (!exist) {
			this.children.add(node);
		} else {
			// update all child if exists
			Node.previousItems.clear();
			Node.previousItems.addAll(node.items);
			//System.out.println("All previous" + Node.previousItems);
		}
	}

	/**
	 * Add new element (for the composed item)
	 * 
	 * @param element
	 *            elements belongs to the focal element being treated
	 * @param item
	 *            item object
	 * @param next
	 *            the next value in the focal element
	 * @return a node object corresponding to the selected one
	 */
	public Node addElement(String element, Item item, String next) {
		Node node = this.getNode(element);
		ArrayList<Item> list = new ArrayList<Item>();
		if (next.equals("")) {
			// update the belief of the item
			for (Item i : Node.previousItems) {
				if (i.rowid == item.rowid) {
					item.belief += i.belief;
				}
			}
			if(node!=null){
				node.items.add(item);
			}
			list.add(item);
		}
		if (node == null) {
			node = new Node(element, list);
			this.children.add(node);
		} else {
			Node.previousItems.addAll(node.items);

		}
		return node;
	}
	
	/**
	 * Search for a node
	 * 
	 * @param searchValue
	 *            the label of the node looking for
	 * @return a node object having the specified label
	 */
	public Node getNode(String searchValue) {
		searchValue = searchValue.trim();
		Node result = null;
		if (this.value.equals(searchValue)) {
			return this;
		} else {
			for (Node n : this.children) {
				if(n.value.equals(searchValue)){
					return n;
				}
			}
			return result;
		}
	}

	/**
	 * Search for a node
	 * 
	 * @param searchValue
	 *            the label of the node looking for
	 * @return a node object having the specified label
	 * @deprecated
	 */
	public Node getNodeDep(String searchValue) {
		searchValue = searchValue.trim();
		Node result = null;
		if (this.value.equals(searchValue)) {
			return this;
		} else {
			for (Node n : this.children) {
				result = n.getNode(searchValue);
				if (result != null && result.value.equals(searchValue)) {
					return result;
				}
			}
			return result;
		}
	}

	/**
	 * Update the belief value of the selected node
	 * 
	 * @param node
	 *            selected node
	 */
	public void updateChildrenBelief(Node node) {
		for (Node n : node.children) {
			// verify if the node is a leaf
			if (n.children.size() == 0) {
				// update the belief
				for (Item previous : Node.previousItems) {
					for (Item item : n.items) {
						if (item.rowid == previous.rowid) {
							item.belief += previous.belief;
						}
					}
				}
			} else {
				for (Node child : node.children) {
					updateChildrenBelief(child);
				}
			}
		}
	}

	/**
	 * Convert a node object to string
	 */
	public String toString() {
		String ch = "";
		System.out.print("Node value : " + this.value + " : ");
		for (Item item : this.items) {
			System.out.print(item + " / ");
		}
		if (this.children.size() > 0) {
			System.out.println("Children : ");
			for (Node child : this.children) {
				System.out.println(child);
			}
		}
		return ch;
	}

}
