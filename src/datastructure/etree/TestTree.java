package datastructure.etree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import datastructure.ridlist.RIDLists;

public class TestTree {

	public static void main(String[] args) {
		EvidentialTree tree = new EvidentialTree("datasets/data5.txt", 1);
		// System.out.println(tree.root);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<Item> result = tree.search("1,2,5", 0.0001);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		System.out.println("Spent time : " + (stopTime - startTime)); // execution
																		// time
		Collections.sort(result, new Comparator<Item>() {

			@Override
			public int compare(Item o1, Item o2) {
				if (o1.rowid < o2.rowid) {
					return -1;
				}
				return 1;
			}

		});
		
		RIDLists ridlist = new RIDLists("datasets/data5.txt", 1);
		ArrayList<datastructure.ridlist.Item> res = new ArrayList<datastructure.ridlist.Item>();
		res = ridlist.search("1,2,5", 0.0001);
		System.out.println("Nb : " + res.size());

		for (datastructure.ridlist.Item item : res) {
			boolean exist=false;
			for(Item i:result){
				if(item.rowid==i.rowid){
					exist=true;
					break;
				}
			}
			if(!exist){
				System.out.println(item);
			}
		}
		
		
		
		
//		for (datastructure.etree.Item item : result) {
//			System.out.println(item);
//		}

	}

}
