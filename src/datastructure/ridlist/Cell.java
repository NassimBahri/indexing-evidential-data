package datastructure.ridlist;

import java.util.ArrayList;

/**
 * <strong>The Cell class</strong> stores a list of couples for each entry.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Cell {

	/**
	 * list of items in the cell
	 */
	public ArrayList<Item> items;
	/**
	 * Default constructor of this class
	 */
	public Cell() {
		this.items = new ArrayList<Item>();
	}

	/**
	 * Convert a cell object to String
	 */
	public String toString() {
		String ch = "";
		for (int i = 0; i < this.items.size(); i++) {
			ch += this.items.get(i);
		}
		return ch;
	}

}
