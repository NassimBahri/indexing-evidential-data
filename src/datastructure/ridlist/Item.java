package datastructure.ridlist;

/**
 * This class offers a structure to store the details of the focal elements
 * (belief value and the row identifier). Each item refers to a line number with
 * a belief value.
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * row identifier of the focal element
	 */
	public int rowid;
	/**
	 * mass of the focal element
	 */
	public double mass;
	/**
	 * the belief of the focal element
	 */
	public double belief;

	/**
	 * default constructor of the class Item
	 */
	public Item() {
	}

	/**
	 * Custom constructor for the current class
	 * 
	 * @param rowid
	 *            the row identifier of the item
	 * @param mass
	 *            the mass of the item
	 */
	public Item(int rowid, double mass) {
		this.rowid = rowid;
		this.mass = mass;
		this.belief = mass;
	}

	/**
	 * Convert an item object to String
	 */
	public String toString() {
		String ch = "";
		ch += "(" + this.rowid + " , " + this.belief + ")";
		return ch;
	}

}
