package datastructure.ridlist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import datastructure.util.Collection;

/**
 * <strong>The RID Lists data structure</strong> is a vertical representation of
 * the evidential database. It refers to the inverted index in some domains such
 * as the Information Retrieval. This class allows to create a RID Lists for
 * only one attribute from the evidential database by specifying its position.
 * <br>
 * <br>
 * <strong>Reference:</strong> M. A. Bach Tobji, B. Ben Yaghlane and K.
 * Mellouli. Frequent Itemset Mining from Databases Including One Evidential
 * Attribute, <i>, in Scalable Uncertainty Management: Second International
 * Conference, SUM 2008, Naples, Italy, October 1-3, 2008. Proceedings</i>, eds.
 * S. Greco and T. Lukasiewicz. (Springer Berlin Heidelberg, Berlin, Hei-
 * delberg, 2008), Berlin, Heidelberg, pp. 19–32. <br>
 * <br>
 * <strong>Example of use :</strong> <br>
 * 
 * <pre>
 * // Create the RID Lists
 * RIDLists ridlist = new RIDLists("dataset.txt", 1);
 * 
 * // Insert new line in the RID Lists
 * HashMap&lt;String, Item&gt; newline = new HashMap&lt;String, Item&gt;();
 * newline.put("flu,anemia", new Item(5, 0.7));
 * newline.put("flu", new Item(5, 0.3));
 * ridlist.insert(newline);
 * 
 * // Deleting a line from the RID Lists
 * HashMap&lt;String, Item&gt; oldline = new HashMap&lt;String, Item&gt;();
 * oldline.put("flu,anemia", new Item(5, 0.7));
 * oldline.put("flu", new Item(5, 0.3));
 * ridlist.delete(oldline);
 * 
 * // Search all items with the corresponding properties
 * ArrayList&lt;Item&gt; listItems = ridlist.search("flu");
 * </pre>
 * 
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class RIDLists {

	/**
	 * collection of records stored in the RID Lists
	 */
	public Map<String, Cell> records;

	/**
	 * Constructor of the class RIDLists
	 * 
	 * @param filePath
	 *            the path to the data file
	 * @param attribute
	 *            position of the attribute to treat (we start counting from 1)
	 */
	@SuppressWarnings("resource")
	public RIDLists(String filePath, int attribute) {
		this.records = new HashMap<String, Cell>();
		File file = new File(filePath);
		String elementsLine, massLine; // elements and mass lines readed from
		// the file
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		int currentAttribute = 1;
		double sum = 0;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					if (currentAttribute == attribute) {
						// alphabetic sort
						focalElementValues = elementsList[i].toLowerCase().split(",");
						Arrays.sort(focalElementValues);
						String tmp = String.join(",", focalElementValues);
						this.add(tmp, new Item(lineNumber, Double.parseDouble(massList[i])));
					}
					sum += Double.parseDouble(massList[i]);
					if (sum == 1) {
						sum = 0;
						currentAttribute++;
					}
				}
				currentAttribute = 1; // reset the current attribute
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method for updating the RID Lists by computing the belief of every focal
	 * element in each line
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateBeliefsRidList() {
		/** list of values in a focal element **/
		String[] focalElementCell1Values, focalElementCell2Values;
		String keyCell1, keyCell2; // focal element
		int j; // decimal value of the focal elements
		boolean itemExist;
		// sort the rid list in a decreasing order following to the sizes of the
		// items to avoid multiple updates of the same item.
		Object[] ridListKeys = this.records.keySet().toArray();
		Arrays.sort(ridListKeys, new Comparator() {
			public int compare(Object o1, Object o2) {
				String[] items1 = o1.toString().split(","), items2 = o2.toString().split(",");
				if (items1.length > items2.length) {
					return -1;
				}
				return 1;
			}
		});
		for (Object key : ridListKeys) {
			keyCell1 = key.toString();
			Cell cell1 = this.records.get(key);
			focalElementCell1Values = keyCell1.split(",");
			if (focalElementCell1Values.length == 1) {
				// sort the items within the current cell
				Collections.sort((List<Item>) cell1.items, new Comparator<Item>() {
					@Override
					public int compare(Item o1, Item o2) {
						if (o1.belief > o2.belief) {
							return -1;
						}
						return 1;
					}
				});
				continue;
			}
			for (Entry<String, Cell> cell2 : this.records.entrySet()) {
				keyCell2 = cell2.getKey();
				focalElementCell2Values = keyCell2.split(",");
				if (focalElementCell1Values.length <= focalElementCell2Values.length) {
					continue;
				}
				if (Collection.isSubsetOf(focalElementCell2Values, focalElementCell1Values) == true) {
					// verify if the Rid List contains all the keys
					ArrayList<Item> itemsCell1 = cell1.items, itemsCell2 = cell2.getValue().items;
					// iterate all items included in the cell 2
					for (int i = 0; i < itemsCell2.size(); i++) {
						j = 0;
						itemExist = false;
						while (j < itemsCell1.size() && !itemExist) {
							if (itemsCell2.get(i).rowid == itemsCell1.get(j).rowid) {
								itemsCell1.get(j).belief += itemsCell2.get(i).mass;
								itemExist = true;
							}
							j++;
						}
						if (!itemExist) {
							itemsCell1.add(new Item(itemsCell2.get(i).rowid, itemsCell2.get(i).mass));
						}
					}
				}
			}
			// sort the items within the current cell
			Collections.sort((List<Item>) cell1.items, new Comparator<Item>() {
				@Override
				public int compare(Item o1, Item o2) {
					if (o1.belief > o2.belief) {
						return -1;
					}
					return 1;
				}
			});
		}
	}

	/**
	 * Generate all possible subsets from a focal element
	 * 
	 * @param items
	 *            list of items composing the focal element
	 * @return list of all possible subsets
	 */
	public ArrayList<String> generatePowerSet(String[] items) {
		ArrayList<String> tempList = null, powerSet = new ArrayList<String>();
		for (String item : items) {
			tempList = new ArrayList<String>();
			for (String temp : powerSet) {
				tempList.add(temp + "," + item);
			}
			powerSet.add(item);
			powerSet.addAll(tempList);
		}
		return powerSet;
	}

	/**
	 * Searching for a value in the RID Lists
	 * 
	 * @param value
	 *            focal element to search (specified in the WHERE clause)
	 * @param threshold
	 *            minimum belief
	 * @return a list of items that point to the affected lines
	 */
	public ArrayList<Item> search(String value, double threshold) {
		value = value.toLowerCase();
		String[] focalElements = value.split(","), tempList;
		ArrayList<Item> result = new ArrayList<Item>();
		Cell cell;
		int j;
		boolean itemExist;
		// if the searched value is singleton
		if (focalElements.length == 1) {
			cell = this.records.get(value);
			if (cell != null) {
				// return cell.items;
				for (Item item : cell.items) {
					if (item.belief >= threshold) {
						result.add(item);
					} else {
						break;
					}
				}
				return result;
			}
		} else {
			// if the searched value is a composite
			ArrayList<String> powerSet = this.generatePowerSet(focalElements);
			for (String subset : powerSet) {
				/*tempList = subset.split(",");
				Arrays.sort(tempList);
				String sortedSubset = String.join(",", tempList);
				cell = this.records.get(sortedSubset);*/
				cell = this.records.get(subset);
				if (cell != null) {
					// ridlist.put(sortedSubset,
					// this.records.get(sortedSubset).items);
					for (int i = 0; i < cell.items.size(); i++) {
						j = 0;
						itemExist = false;
						while (j < result.size() && !itemExist) {
							if (cell.items.get(i).rowid == result.get(j).rowid) {
								result.get(j).belief += cell.items.get(i).mass;
								itemExist = true;
							}
							j++;
						}
						if (!itemExist) {
							result.add(new Item(cell.items.get(i).rowid, cell.items.get(i).mass));
						}
					}
				}
			}
			// result = temporaryResult;
			/*
			 * for (Item item : temporaryResult) { if (item.belief >= threshold)
			 * { result.add(item); } }
			 */
		}
		// remove
		int i = 0;
		while (i < result.size()) {
			if (result.get(i).belief < threshold) {
				result.remove(i);
			} else {
				i++;
			}
		}
		return result;
	}

	/**
	 * Searching for a value in the RID Lists without using the hash structure
	 * 
	 * @param value
	 *            focal element to search (specified in the WHERE clause)
	 * @param threshold
	 *            minimum belief
	 * @return a list of items that point to the affected lines
	 */
	public ArrayList<Item> searchLinear(String value, double threshold) {
		value = value.toLowerCase();
		String[] focalElements = value.split(",");
		ArrayList<Item> result = new ArrayList<Item>();
		int j;
		boolean itemExist;
		for (Entry<String, Cell> cell : this.records.entrySet()) {
			String keyCell = cell.getKey();
			String[] cellValues = keyCell.split(",");
			if (cellValues.length > focalElements.length) {
				continue;
			} else if (Collection.isSubsetOf(cellValues, focalElements)) {
				for (int i = 0; i < cell.getValue().items.size(); i++) {
					j = 0;
					itemExist = false;
					while (j < result.size() && !itemExist) {
						if (cell.getValue().items.get(i).rowid == result.get(j).rowid) {
							result.get(j).belief += cell.getValue().items.get(i).mass;
							itemExist = true;
						}
						j++;
					}
					if (!itemExist) {
						result.add(new Item(cell.getValue().items.get(i).rowid, cell.getValue().items.get(i).mass));
					}
				}
			}
		}
		return result;
	}

	/**
	 * Insert new line in the RID Lists
	 * 
	 * @param items
	 *            list of items corresponding to a specified focal element
	 */
	@SuppressWarnings("rawtypes")
	public void insert(Map<String, Item> items) {
		String[] focalElements;
		ArrayList<Item> result = new ArrayList<Item>();
		String itemKey = "";
		boolean exist = false;
		Cell cell = null;
		// update the belief value of each focal element in the list
		for (Entry item : items.entrySet()) {
			for (Entry tempItem : items.entrySet()) {
				if (!item.getKey().toString().equals(tempItem.getKey().toString()) && Collection
						.isSubsetOf(tempItem.getKey().toString().split(","), item.getKey().toString().split(","))) {
					((Item) item.getValue()).belief += ((Item) tempItem.getValue()).mass;
				}
			}
		}
		// iterate on the new line's focal elements
		for (Entry item : items.entrySet()) {
			focalElements = item.getKey().toString().split(",");
			Arrays.sort(focalElements); // alphabetic sort
			itemKey = String.join(",", focalElements);
			result.add((Item) item.getValue());
			// iterate on the rid list structure
			for (Entry record : this.records.entrySet()) {
				if (focalElements.length < record.getKey().toString().split(",").length
						|| itemKey.equals(record.getValue().toString())) {
					continue;
				} else if (focalElements.length > record.getKey().toString().split(",").length
						&& Collection.isSubsetOf(record.getKey().toString().split(","), focalElements)) {
					for (Item itm : ((Cell) record.getValue()).items) {
						exist = false;
						for (Item stored : result) {
							if (stored.rowid == itm.rowid) {
								exist = true;
								stored.belief += itm.mass;
								break;
							}
						}
						if (!exist) {
							result.add(new Item(itm.rowid, itm.mass));
						}
					}
				}
			}
			// add the result to the existing rid list
			if (this.records.get(itemKey) == null) {
				cell = new Cell();
				for (Item itm : result) {
					cell.items.add(new Item(itm.rowid, itm.belief));
				}
				this.records.put(itemKey, cell);
			} else {
				for (Item itm : result) {
					this.records.get(itemKey).items.add(new Item(itm.rowid, itm.belief));
				}
			}
			result.clear();
		}
	}

	/**
	 * Deleting a line from the RID Lists
	 * 
	 * @param items
	 *            list of items corresponding to a specified focal element
	 */
	@SuppressWarnings("rawtypes")
	public void delete(Map<String, Item> items) {
		String[] focalElements;
		String itemKey = "";
		// update the belief value of each focal element in the list
		for (Entry item : items.entrySet()) {
			for (Entry tempItem : items.entrySet()) {
				if (!item.getKey().toString().equals(tempItem.getKey().toString()) && Collection
						.isSubsetOf(tempItem.getKey().toString().split(","), item.getKey().toString().split(","))) {
					((Item) item.getValue()).belief += ((Item) tempItem.getValue()).mass;
				}
			}
		}
		// iterate on the new line's focal elements
		for (Entry item : items.entrySet()) {
			focalElements = item.getKey().toString().split(",");
			Arrays.sort(focalElements); // alphabetic sort
			itemKey = String.join(",", focalElements);
			// iterate on the rid list structure
			for (Entry record : this.records.entrySet()) {
				if (focalElements.length > record.getKey().toString().split(",").length) {
					continue;
				} else if (itemKey.equals(record.getValue().toString())
						|| Collection.isSubsetOf(record.getKey().toString().split(","), focalElements)) {
					int i = 0;
					while (i < ((Cell) record.getValue()).items.size()) {
						Item itm = ((Cell) record.getValue()).items.get(i);
						if (((Item) item.getValue()).rowid == itm.rowid) {
							itm.belief -= ((Item) item.getValue()).belief;
						}
						if (itm.belief == 0) {
							((Cell) record.getValue()).items.remove(itm);
						}
						i++;
					}
				}
			}
		}
	}

	/**
	 * Add new item to the cell corresponding to the specified focal element
	 * 
	 * @param key
	 *            focal element
	 * @param item
	 *            item to insert
	 */
	public void add(String key, Item item) {
		if (!this.records.containsKey(key)) {
			this.records.put(key, new Cell());
		}
		this.records.get(key).items.add(item);
		// sort the items within the current cell
		Collections.sort((List<Item>) this.records.get(key).items, new Comparator<Item>() {
			@Override
			public int compare(Item o1, Item o2) {
				if (o1.belief > o2.belief) {
					return -1;
				}
				return 1;
			}
		});
	}

	/**
	 * Convert a RID Lists object to String
	 */
	public String toString() {
		String ch = "";
		for (java.util.Map.Entry<String, Cell> cell : this.records.entrySet()) {
			ch += cell.getKey() + "\n";
			ch += cell.getValue();
			ch += "\n";
		}
		return ch;
	}

}
