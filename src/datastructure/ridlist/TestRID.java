package datastructure.ridlist;

import java.util.ArrayList;

public class TestRID {

	public static void main(String[] args) {
		RIDLists ridlist = new RIDLists("datasets/data5.txt", 1);
		ArrayList<Item> res = new ArrayList<Item>();
		long startTime = System.currentTimeMillis(); // start time
		// res = ridlist.search("Mer,SQL,Ad", 0);
		res = ridlist.search("1,2,5", 0.0001);
		System.out.println("Nb : " + res.size());
		long stopTime = System.currentTimeMillis(); // end time
		System.out.println("Spent time : " + (stopTime - startTime)); // execution
																		// time

		// Collections.sort(res, new Comparator<Item>() {
		//
		// @Override
		// public int compare(Item o1, Item o2) {
		// if (o1.rowid < o2.rowid) {
		// return -1;
		// }
		// return 1;
		// }
		//
		// });
		// for (datastructure.ridlist.Item item : res) {
		// System.out.println(item);
		// }

	}

}
