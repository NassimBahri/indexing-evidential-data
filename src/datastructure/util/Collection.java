package datastructure.util;

/**
 * <strong>Collection class</strong> consists exclusively of static methods that
 * operate on or return collections.
 * 
 * 
 * @author Nassim BAHRI
 * @author Mohamed Anis BACH TOBJI
 * @version 1.0
 *
 */
public class Collection {

	/**
	 * Verify if list2 is a subset of list1
	 * 
	 * @param list2
	 *            Specifies the list to search
	 * @param list1
	 *            Specifies where to begin the search
	 * @return
	 */
	public static boolean isSubsetOf(String[] list2, String[] list1) {
		boolean exist;
		int i;
		for (String elem2 : list2) {
			exist = false;
			i = 0;
			while (i < list1.length && !exist) {
				if (elem2.trim().equals(list1[i].trim())) {
					exist = true;
				}
				i++;
			}
			if (!exist) {
				return false;
			}
		}
		return true;
	}

}
