/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidentialDatabase;

import java.util.*;
/**
 *
 * @author Anis
 */
public class BBA
{
    ArrayList<FocalElement> content = new ArrayList<>();
    int attribute;

    
    public void generate_generic_bba(int attr, int max_nfe, int max_sfe, int card)
    {
        //Generates a random number of the bba's focal elements
        //... between 1 and max_nfe
        int nfe = Random.getRandInt(max_nfe);
        int sfe;
        int item;
        double mass;
        double rest;
        double sum = 0;
        
        this.attribute = attr;
        
        EDB.displayln("nfe"+nfe);
        
        for(int i=1;i<=nfe;i++){
            //Generates a random size for each focal element
            //... that is different from 1, if the generated nfe is yet equal to one
            //... to avoid the perfect case
            do{
                sfe = Random.getRandInt(max_sfe);
            }while((nfe==1)&&(sfe==1));
            
            rest = 1;
            EDB.displayln("sfe"+sfe);
            
            FocalElement fe = new FocalElement();
            
            for(int j=1;j<=sfe;j++){
                
                item = Random.getRandInt(card);

                //Add a non existing item in the focal element
                while(!fe.add_distinct(item)){
                    item = Random.getRandInt(card);
                }
            }
            if (i!=nfe){
                mass = Random.getMass(rest);
                rest = rest - mass;
                sum += mass;
            }else{
                mass = Random.Round(1 - sum,2);
            }
            
            fe.mass = mass;
            Collections.sort(fe.content);
            if (!this.add_distinct(fe)){
                i--;
            }           
        }
        
    }
    
    public void generate_perfect_bba(int attr, int card){
        
        FocalElement fe = new FocalElement();
        int item = Random.getRandInt(card);
        fe.content.add(item);
        fe.mass = 1;
        this.content.add(fe);
        this.attribute = attr;
    }
    
    
    private boolean add_distinct(FocalElement fe)
    {
        int i=0;
        boolean distinct = true;
        
        while ((distinct) && (i<this.content.size()))
        {
            if(fe.content.equals(this.content.get(i).content))
            {
                distinct = false;
            }
            i++;
        }
        if (distinct)
        {
            this.content.add(fe);
        }
        return distinct;
    }
    
    public String toStr()
    {
        String ch="Attribute: "+this.attribute+"\n";
        for(FocalElement fe: this.content)
        {
            ch += fe.toStr();
        }
        return ch;
    }    
    
}
