package evidentialDatabase;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anis
 */
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class EDB {
	public ArrayList<Record> content = new ArrayList<>();

	static long d = 10000; // number of generated records
	static int max_nfe = 4; // max number of focal element in a record
	static int max_sfe = 3; // max size of the focal element
	static final int attributes = 1;
	// static int card[] = new int[attributes];
	static double pct_imperfection = 0.70;

	static int card[] = { 100000 };

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {

		// init params
		//d = Long.parseLong(args[0]);
		//max_nfe = Integer.parseInt(args[1]);
		//max_sfe = Integer.parseInt(args[2]);
		//String filename = args[3];
		String filename="datasets/data6.txt";
		
		EDB gedb = new EDB();

		gedb.generate_synthetic_edb();
		gedb.display();

		File fw = new File(filename);
		gedb.save_in_file(fw);
		System.out.println("finish");

		/*
		 * BBA x = new BBA(); x.generate_generic_bba(1, 5, 3, 1000);
		 * EDB.displayln(x.toStr());
		 */

		/*
		 * Record y = new Record(); y.generate_record(false, card, attributes,
		 * max_nfe, max_sfe); EDB.displayln(y.toStr());
		 */

	}

	public void generate_synthetic_edb() {

		boolean perfect_record;
		// The variable "val" is used to ditribute imperfect records in the
		// database
		// ... according to the "pct_imperfection" rate
		double val = Random.Round(1 / pct_imperfection, 4);
		@SuppressWarnings("unused")
		int count = 0;
		for (long i = 1; i <= d; i++) {
			Record rec = new Record();
			if (i % val < 1) {
				perfect_record = false;
				count++;
			} else {
				perfect_record = true;
			}
			// Generates a perfect/imperfect record, according to the value
			// ... of "perfect_record"
			rec.generate_record(perfect_record, card, attributes, max_nfe, max_sfe);
			rec.id = i;
			this.content.add(rec);
		}

	}

	public void save_in_file(File fl) {

		String line1 = "";
		String line2 = "";
		String chfile = "";
		FileWriter fw;

		try {
			for (Record r : this.content) {
				for (BBA b : r.content) {
					for (FocalElement f : b.content) {
						line1 += f.itemtoStr();
						line2 += f.mass + " ";
					}
				}
				chfile += line1 + "\n" + line2 + "\n";
				line1 = "";
				line2 = "";
			}
			displayln(chfile);

			fw = new FileWriter(fl);
			fw.write(chfile);

			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void displayln(Object arg) {
		System.out.println(arg);
	}

	public void display() {
		for (Record rec : this.content) {
			displayln(rec.toStr());
		}
	}

}
