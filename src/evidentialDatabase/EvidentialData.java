package evidentialDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class EvidentialData {

	private String filePath;

	public static void main(String[] args) {
		EvidentialData data = new EvidentialData("datasets/trivial-data.txt");
		ArrayList<Item> result = data.search("flu,cancer,anemia", 0);
		System.out.println(result);
	}

	public EvidentialData(String filePath) {
		this.filePath = filePath;
	}

	@SuppressWarnings({ "resource" })
	public ArrayList<Item> search(String value, double threshold) {
		String[] focalElementsSearched = value.split(",");
		int focalElementsSearchedSize = focalElementsSearched.length;
		File file = new File(this.filePath);
		String[] elementsList, massList; // elements as mass list
		String[] focalElementValues; // focal element values
		String elementsLine, massLine; // elements and mass lines readed from
		ArrayList<Item> result = new ArrayList<Item>();
		boolean exist;
		// the file
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1; // line number
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(" ");
				massList = massLine.split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					focalElementValues = elementsList[i].split(",");
					if (focalElementValues.length > focalElementsSearchedSize) {
						continue;
					}
					if (focalElementsSearchedSize == 1 && value.equals(elementsList[i])
							&& Double.parseDouble(massList[i]) >= threshold) {
						result.add(new Item(lineNumber, Double.parseDouble(massList[i])));
						break;
					}
					if (this.isSubsetOf(focalElementValues, focalElementsSearched)) {

						// verify if the row id is already exist
						exist = false;
						for (Item res : result) {
							if (res.rowid == lineNumber) {
								exist = true;
								res.belief += Double.parseDouble(massList[i]);
								break;
							}
						}
						if (!exist) {
							result.add(new Item(lineNumber, Double.parseDouble(massList[i])));
						}
					}
				}
				lineNumber++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * verify if the set list2 is a subset of list1
	 * 
	 * @param list2
	 * @param list1
	 * @return
	 */
	private boolean isSubsetOf(String[] list2, String[] list1) {
		boolean exist;
		int i;
		for (String elem2 : list2) {
			exist = false;
			i = 0;
			while (i < list1.length && !exist) {
				if (elem2.trim().equals(list1[i].trim())) {
					exist = true;
				}
				i++;
			}
			if (!exist) {
				return false;
			}
		}
		return true;
	}

}
