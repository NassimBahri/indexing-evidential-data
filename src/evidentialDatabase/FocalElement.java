/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidentialDatabase;

/**
 *
 * @author Anis
 */
import java.util.ArrayList;

public class FocalElement
{
    ArrayList<Integer> content = new ArrayList<>();
    long bin_code;
    double mass;
    double belief;
    
    //Adds an item to the focal element if it does not exist in
    public boolean add_distinct(int item)
    {
        int i=0;
        boolean distinct = true;
        
        while ((distinct) && (i<this.content.size()))
        {
            if(item == this.content.get(i))
            {
                distinct = false;
            }
            i++;
        }
        if (distinct)
        {
            this.content.add(item);
        }
        return distinct;
    }
    
    public String toStr()
    {
        String ch = "";
        for(int i=0;i<this.content.size();i++)
        {
            ch += this.content.get(i);
            ch += i!=this.content.size()-1 ? ",":"";
        }
        ch += " mass=" + this.mass + "\n";
        return ch;
    }
    
    public String itemtoStr()
    {
        String ch = "";
        for(int i=0;i<this.content.size();i++)
        {
            ch += this.content.get(i);
            ch += i!=this.content.size()-1 ? ",":"";
        }
        ch += " ";
        return ch;
    }
}
