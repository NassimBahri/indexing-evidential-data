package evidentialDatabase;

public class Item {

	/**
	 * row id
	 */
	long rowid;
	/**
	 * belief of the focal element
	 */
	double belief;

	/**
	 * custom constructor of the Result class
	 * 
	 * @param rowid
	 * @param belief
	 */
	public Item(long rowid, double belief) {
		this.rowid = rowid;
		this.belief = belief;
	}

	/**
	 * convert object to string
	 */
	public String toString() {
		return "(" + this.rowid + " , " + this.belief + ")";
	}
}