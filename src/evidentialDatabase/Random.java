package evidentialDatabase;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anis
 */
import java.text.DecimalFormat;

public class Random{
    
    //Generates a non-zero random value between 0.0 and 1.0
    private static double getRand()
    {
        double rand;
        do
        {
            rand = Math.random();
        }while(Round(rand,2) == 0.0);
        return rand;
    }
    
    public static double getMass(double rest)
    {
        double mass = getRand();
        //Ganerates a non-zero random value between 0.0 and "rest/2" to avoid
        //... making a very small range for the next random value
        mass = mass*rest/2;
               
        return Round(mass,2);
    }
    
    //Generates a random integer between 1 and max
    public static int getRandInt(int max)
    {
        return (int)Math.ceil(getRand()*max);
    }
    
    public static double Round(double val, int precision)
    {
        String str_precision="#.";
        for(int i=1;i<=precision;i++){
            str_precision += "#";
        }
        //Rounds the generated random value to "precision" decimal places
        DecimalFormat df = new DecimalFormat(str_precision);
        //... and replaces the comma, in the concevrted value, by a point
        String str_val = df.format(val).replace(',','.');
        //... else we cannot not reconvert it to double
        return Double.parseDouble(str_val);       
    }
        
}
