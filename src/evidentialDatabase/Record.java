/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidentialDatabase;

import java.util.ArrayList;
/**
 *
 * @author Anis
 */
public class Record
{
    ArrayList<BBA> content = new ArrayList<>();
    long id;
    
    
    public void generate_record(boolean perfect, int card[], int attributes, int nfe, int sfe){
        
        for(int i=0;i<attributes;i++){
            BBA gbba = new BBA();
            if (perfect){
                gbba.generate_perfect_bba(i+1,card[i]);
            }else{
                gbba.generate_generic_bba(i+1,nfe,sfe,card[i]);
            }
            this.content.add(gbba);
        }       
    }
    
     public String toStr()
    {
        String ch="Record Id:"+this.id+"\n";
        for(BBA b: this.content)
        {
            ch += b.toStr();
        }
        return ch;
    }
}
