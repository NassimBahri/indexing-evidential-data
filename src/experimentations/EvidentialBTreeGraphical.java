package experimentations;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import datastructure.btree.EvidentialBTree;
import datastructure.btree.Item;
import datastructure.btree.Node;

public class EvidentialBTreeGraphical extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTree arbre;

	/**
	 * constructor of the current class
	 * 
	 * @param node
	 */
	public EvidentialBTreeGraphical(Node node) {
		this.setSize(800, 700);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Evidential BTree Structure");
		DefaultMutableTreeNode racine = new DefaultMutableTreeNode(
				"∅ (max : " + node.maxBel + "/ min : " + node.minBel + ")");
		if (node.entries.size() > 0) {
			buildTree(racine, node, true);
		} else {
			buildTree(racine, node);
		}
		arbre = new JTree(racine);
		expandAllNodes(arbre, 0, arbre.getRowCount());
		this.getContentPane().add(new JScrollPane(arbre));
		this.setVisible(true);
	}

	/**
	 * recursive method for tree building
	 * 
	 * @param racine
	 * @param node
	 */
	private void buildTree(DefaultMutableTreeNode racine, Node node) {
		// Nous allons ajouter des branches et des feuilles à notre racine
		for (Node child : node.children) {
			DefaultMutableTreeNode rep = new DefaultMutableTreeNode(
					"(max : " + child.maxBel + "/ min : " + child.minBel + ")");
			if (child.children.size() > 0) {
				buildTree(rep, child);
			} else if (child.entries.size() > 0) {
				buildTree(rep, child, true);
			}
			racine.add(rep);
		}

	}

	private void buildTree(DefaultMutableTreeNode racine, Node node, boolean entries) {
		// Nous allons ajouter des branches et des feuilles à notre racine
		for (Item child : node.entries) {
			DefaultMutableTreeNode rep = new DefaultMutableTreeNode(
					"(" + child.focalElement + ")" + child.belief + " -> " + child.rowid);
			racine.add(rep);
		}

	}
	
	private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
	    for(int i=startingIndex;i<rowCount;++i){
	        tree.expandRow(i);
	    }

	    if(tree.getRowCount()!=rowCount){
	        expandAllNodes(tree, rowCount, tree.getRowCount());
	    }
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		EvidentialBTree bTree = new EvidentialBTree("datasets/data.txt", 1, 2);
		EvidentialBTreeGraphical fen = new EvidentialBTreeGraphical(bTree.root);
	}
}