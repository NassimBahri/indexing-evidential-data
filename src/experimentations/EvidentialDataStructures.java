package experimentations;

import java.util.ArrayList;

import datastructure.alternativeBitmap.EvidentialBitmapOptimized;
import datastructure.alternativeBitmap.Item;
import datastructure.bitmap.LinkedEvidentialBitmap;
import datastructure.bitmap.Result;
import datastructure.btree.EvidentialBTree;
import datastructure.etree.EvidentialTree;
import datastructure.ridlist.RIDLists;
import evidentialDatabase.EvidentialData;

public class EvidentialDataStructures {

	private static String focalElement;
	private static double bel;

	public static void main(String[] args) {
		long time = 0;
		focalElement = args[3];
		bel = Double.parseDouble(args[4]);
		switch (args[0]) {
		case "ridlist":
			time = ridListTest(args[1], Integer.parseInt(args[2]));
			break;
		case "linear":
			time = ridListLinearTest(args[1], Integer.parseInt(args[2]));
			break;
		case "etree":
			time = eTreeTest(args[1], Integer.parseInt(args[2]));
			break;
		case "bitmap":
			time = bitmapTest(args[1], Integer.parseInt(args[2]));
			break;
		case "alt":
			time = bitmapAltTest(args[1], Integer.parseInt(args[2]));
			break;
		case "noindex":
			time = noIndexTest(args[1], Integer.parseInt(args[2]));
			break;
		case "btree":
			time = bTreeTest(args[1], Integer.parseInt(args[2]), Integer.parseInt(args[5]));
			break;
		}
		System.out.println("Time spent : " + time);
	}

	// testing ridlist
	private static long ridListTest(String filepath, int attribute) {
		RIDLists ridlist = new RIDLists(filepath, attribute);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<datastructure.ridlist.Item> result = ridlist.search(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing linear ridlist
	private static long ridListLinearTest(String filepath, int attribute) {
		RIDLists ridlist = new RIDLists(filepath, attribute);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<datastructure.ridlist.Item> result = ridlist.searchLinear(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing eTree
	private static long eTreeTest(String filepath, int attribute) {
		EvidentialTree tree = new EvidentialTree(filepath, attribute);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<datastructure.etree.Item> result = tree.search(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing bitmap
	private static long bitmapTest(String filepath, int attribute) {
		LinkedEvidentialBitmap bitmap = new LinkedEvidentialBitmap(filepath, attribute);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<Result> result = bitmap.search(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing without index
	private static long noIndexTest(String filepath, int attribute) {
		EvidentialData data = new EvidentialData(filepath);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<evidentialDatabase.Item> result = data.search(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing bitmap
	private static long bTreeTest(String filepath, int attribute, int order) {
		EvidentialBTree tree = new EvidentialBTree(filepath, attribute, order);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<datastructure.btree.Item> listItems = new ArrayList<datastructure.btree.Item>();
		listItems = tree.search(tree.root, focalElement, bel, listItems);
		System.out.println("Nb : " + listItems.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

	// testing alternative bitmap
	private static long bitmapAltTest(String filepath, int attribute) {
		EvidentialBitmapOptimized bitmap = new EvidentialBitmapOptimized(filepath, attribute);
		long startTime = System.currentTimeMillis(); // start time
		ArrayList<Item> result = bitmap.search(focalElement, bel);
		System.out.println("Nb : " + result.size());
		long stopTime = System.currentTimeMillis(); // end time
		return stopTime - startTime; // execution time
	}

}
