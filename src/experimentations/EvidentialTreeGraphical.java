package experimentations;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import datastructure.etree.EvidentialTree;
import datastructure.etree.Node;

public class EvidentialTreeGraphical extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTree arbre;

	/**
	 * constructor of the current class
	 * @param node
	 */
	public EvidentialTreeGraphical(Node node) {
		this.setSize(800, 700);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("BitTree sctructure");
		DefaultMutableTreeNode racine = new DefaultMutableTreeNode("∅");
		buildTree(racine,node);
		arbre = new JTree(racine);
		this.getContentPane().add(new JScrollPane(arbre));
		this.setVisible(true);
	}

	/**
	 * recursive method for tree building
	 * @param racine
	 * @param node
	 */
	private void buildTree(DefaultMutableTreeNode racine,Node node) {
		// Nous allons ajouter des branches et des feuilles à notre racine
		for(Node child:node.children){
			DefaultMutableTreeNode rep = new DefaultMutableTreeNode(child.value+"("+child.items+")");
			if(child.children.size()>0){
				buildTree(rep, child);
			}
			racine.add(rep);
		}
		
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		EvidentialTree bTree=new EvidentialTree("datasets/count.txt",1);
		EvidentialTreeGraphical fen = new EvidentialTreeGraphical(bTree.root);
	}
}