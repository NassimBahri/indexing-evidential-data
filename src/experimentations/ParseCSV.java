package experimentations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ParseCSV {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		File file = new File("datasets/benchmark.csv");
		File out = new File("datasets/educational.txt");
		PrintWriter writer;
		String elementsLine;
		String[] elementsList;
		InputStream inputStream;
		String content="";
		try {
			inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			
			FileWriter fw = new FileWriter(out.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			int index = 1;
			String elem="";
			while ((elementsLine = buffer.readLine()) != null) {
				elementsList = elementsLine.split(";");
				elem=elementsList[elementsList.length - 1];
				if(index%2==0){
					elem=elem.replace(",", ".");
				}
				content+=(elem)+"\n";
				if (!elementsLine.equals("")) {
					System.out.println(elem);
				}
				index++;
			}
			bw.write(content);
			System.out.println("finish");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
