package experimentations;

import java.util.ArrayList;

import datastructure.alternativeBitmap.EvidentialBitmapOptimized;
import datastructure.alternativeBitmap.Item;
import datastructure.bitmap.LinkedEvidentialBitmap;
import datastructure.bitmap.Result;
import datastructure.btree.EvidentialBTree;
import datastructure.etree.EvidentialTree;
import datastructure.ridlist.RIDLists;
import evidentialDatabase.EvidentialData;

public class PerformanceDataStructure {

	private static final long KELOBYTE = 1024L;
	private static final long MEGABYTE = 1024L * 1024L;

	public static long bytesToKelobytes(long bytes) {
		return bytes / KELOBYTE;
	}
	
	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public static void main(String[] args) {
		long time = 0;
		switch (args[0]) {
		case "ridlist":
			time = ridListTest(args[1], Integer.parseInt(args[2]));
			break;
		case "linear":
			time = ridListLinearTest(args[1], Integer.parseInt(args[2]));
			break;
		case "etree":
			time = eTreeTest(args[1], Integer.parseInt(args[2]));
			break;
		case "bitmap":
			time = bitmapTest(args[1], Integer.parseInt(args[2]));
			break;
		case "alt":
			time = bitmapAltTest(args[1], Integer.parseInt(args[2]));
			break;
		case "noindex":
			time = noIndexTest(args[1], Integer.parseInt(args[2]));
			break;
		case "btree":
			time = bTreeTest(args[1], Integer.parseInt(args[2]), Integer.parseInt(args[5]));
			break;
		}
		System.out.println("Size : " + PerformanceDataStructure.bytesToKelobytes(time));
	}

	// testing ridlist
	private static long ridListTest(String filepath, int attribute) {
		new RIDLists(filepath, attribute);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing linear ridlist
	private static long ridListLinearTest(String filepath, int attribute) {
		new RIDLists(filepath, attribute);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing eTree
	private static long eTreeTest(String filepath, int attribute) {
		new EvidentialTree(filepath, attribute);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing bitmap
	private static long bitmapTest(String filepath, int attribute) {
		new LinkedEvidentialBitmap(filepath, attribute);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing without index
	private static long noIndexTest(String filepath, int attribute) {
		new EvidentialData(filepath);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing bitmap
	private static long bTreeTest(String filepath, int attribute, int order) {
		new EvidentialBTree(filepath, attribute, order);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}

	// testing alternative bitmap
	private static long bitmapAltTest(String filepath, int attribute) {
		new EvidentialBitmapOptimized(filepath, attribute);
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		return memory;
	}
}
